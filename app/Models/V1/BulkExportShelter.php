<?php

namespace App\Models\V1;

use Illuminate\Database\Eloquent\Model;

class BulkExportShelter extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shelter_catalog.shelters_active';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'shelter_id';
}

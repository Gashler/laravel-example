<?php

namespace App\Models\V1;

use Illuminate\Database\Eloquent\Model;

class Shelter extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shelter_catalog.shelters_with_deleted';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'shelter_id';

    /**
     * The validation rules for the model.
     *
     * @var string
     */
    protected $rules = [
        //
    ];

    /**
     * Relationships
     */
    public function pets()
    {
        return $this->hasMany('App\Models\V1\Pet', 'shelter_id');
    }
}

<?php

namespace App\Models\V1;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    protected $fillable = [
        'age',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pet_catalog.pets';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'pet_id';

    /**
     * The validation rules for the model.
     *
     * @var string
     */
    protected $rules = [
        //
    ];
}

<?php

namespace App\Models\V1;

use Illuminate\Database\Eloquent\Model;

class BulkExportPet extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pet_catalog.pets';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'pet_id';
}

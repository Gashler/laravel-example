<?php

namespace App\Models\V2;

use App\Models\V2\BulkExportPetInterface;
use DB;
use Illuminate\Database\Eloquent\Model;

class BulkExportPet extends Model implements BulkExportPetInterface
{
    protected $fillable = [
        'age',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pet_catalog.pets';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'pet_id';

    /**
     * The validation rules for the model.
     *
     * @var string
     */
    protected $rules = [
        //
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'contactName',
        'contactPhone',
        'videoUrls',
        'breed',
        'pictures',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'pictures' => 'array',
    ];

    /**
     * Fix contact name
     *
     * @return string
     */
    public function getContactNameAttribute(): string
    {
        $name = "$this->pet_contact_first_name $this->pet_contact_last_name";
        unset($this->pet_contact_first_name);
        unset($this->pet_contact_last_name);

        return $name;
    }

    /**
     * Fix contact name
     *
     * @return string
     */
    public function getContactPhoneAttribute(): string
    {
        $contactPhone = '';

        if (isset($this->pet_contact_phone_number)) {
            $contactPhone = "($this->pet_contact_phone_area_code) $this->pet_contact_phone_number";
            $contactPhone .= isset($this->pet_contact_phone_extension) ?
                "x$this->pet_contact_phone_extension" : '';
            unset($this->pet_contact_phone_area_code);
            unset($this->pet_contact_phone_number);
            unset($this->pet_contact_phone_extension);
        }

        return $contactPhone;
    }

    /**
     * Fix video URL
     *
     * @return array
     */
    public function getVideoUrlsAttribute(): array
    {
        $videoUrls = [];

        if (isset($this->youtube_video_id)) {
            $videoUrls = [
                'youtubeVideoUrl' => "https://www.youtube.com/watch?v=$this->youtube_video_id",
                'youtubeID' => $this->youtube_video_id
            ];
            unset($this->youtube_video_id);
        }

        return $videoUrls;
    }

    /**
     * Generate breed string
     *
     * @return string
     */
    public function getBreedAttribute(): string
    {
        $breed = '';
        $breed .= $this->primary_breed ?? '';
        $breed .= isset($this->secondary_breed) ? " / $this->secondary_breed" : '';
        $breed .= $this->mixed ? " / Mixed" : '';
        $breed .= isset($this->coat_length) ? " ($this->coat_length coat)" : '';

        return $breed;
    }

    /**
     * Generate photo structure
     *
     * @return string
     */
    public function getPicturesAttribute(): string
    {
        return DB::table(DB::raw('photos p'))
            ->select(
                DB::raw('p.rank AS "rank"'),
                DB::raw('round(date_part(' . "'epoch'" . ', p.uploaded_timestamp)) as "uploaded_epoch"'),
                DB::raw('p.photo_id AS "p_id"'),
                DB::raw('p.photo_width AS "p_width"'),
                DB::raw('p.photo_height AS "p_height"'),
                DB::raw('p.photo_extension AS "p_ext"'),
                DB::raw('a.photo_id AS "a_id"'),
                DB::raw('a.photo_width AS "a_width"'),
                DB::raw('a.photo_height AS "a_height"'),
                DB::raw('a.photo_extension AS "a_ext"'),
                DB::raw('d.photo_id AS "d_id"'),
                DB::raw('d.photo_width AS "d_width"'),
                DB::raw('d.photo_height AS "d_height"'),
                DB::raw('d.photo_extension AS "d_ext"'),
                DB::raw('e.photo_id AS "e_id"'),
                DB::raw('e.photo_width AS "e_width"'),
                DB::raw('e.photo_height AS "e_height"'),
                DB::raw('e.photo_extension as e_ext')
            )
            ->join(
                DB::raw('photos a'),
                function ($join) {
                    $join->on('p.entity_id', '=', 'a.entity_id');
                    $join->on('p.rank', '=', 'a.rank');
                    $join->on('a.tag', '=', DB::raw("'a'"));
                }
            )
            ->join(
                DB::raw('photos d'),
                function ($join) {
                    $join->on('p.entity_id', '=', 'd.entity_id');
                    $join->on('p.rank', '=', 'd.rank');
                    $join->on('d.photo_width', '=', DB::raw(65));
                    $join->on('d.photo_height', '=', DB::raw(65));
                }
            )
            ->join(
                DB::raw('photos e'),
                function ($join) {
                    $join->on('p.entity_id', '=', 'e.entity_id');
                    $join->on('p.rank', '=', 'e.rank');
                    $join->whereBetween('e.photo_width', [123, 125]);
                }
            )
            ->where(
                [
                    'p.entity_type' => 'pets',
                    'p.tag' => 'parent',
                    'p.entity_id' => $this->animalID
                ]
            )
            ->orderBy('p.rank')
            ->get()
            ->map(
                function (object $photo) {
                    $originalUrl = config('api.pet_uploads_url') . '/' .
                        $this->getImageLocation($photo->p_id, $photo->p_ext) . '?' . $photo->p_width . 'x'
                        . $photo->p_height;
                    $urlA = config('api.pet_uploads_url') . '/' .
                        $this->getImageLocation($photo->a_id, $photo->a_ext) . '?' . $photo->a_width . 'x'
                        . $photo->a_height;
                    $urlB = config('api.pet_uploads_url') . '/' .
                        $this->getImageLocation($photo->d_id, $photo->d_ext) . '?' . $photo->d_width . 'x'
                        . $photo->d_height;
                    $urlC = config('api.pet_uploads_url') . '/' .
                        $this->getImageLocation($photo->e_id, $photo->e_ext) . '?' . $photo->e_width . 'x'
                        . $photo->e_height;

                    return [
                        'originalUrl'  => $originalUrl,
                        'fullsizeUrl'  => $originalUrl,
                        'largeUrl'     => $urlA,
                        'thumbnailUrl' => $urlB,
                        'smallUrl'     => $urlC,
                        'mediaOrder'   => $photo->rank,
                        'lastUpdated'  => $photo->uploaded_epoch
                    ];
                }
            );
    }

    /**
     * Build image location string
     *
     * @param integer $id
     * @param string $ext
     * @return string
     */
    public function getImageLocation(int $id, string $ext): string
    {
        $md5 = md5($id);
        $md5 = "$md5[0]/$md5[1]/$md5[2]";
        return "$md5/$id.$ext";
    }

    /**
     * Fix bool fields
     *
     * @return BulkExportPET
     */
    public function fixBoolFields(): BulkExportPet
    {
        $boolFields = [
            'specialNeeds', 'uptodate', 'housetrained', 'declawed', 'kids', 'dogs', 'cats',
            'altered', 'ownerExperience', 'courtesy', 'needsFoster', 'found', 'yardRequired',
            'olderKidsOnly', 'noSmallDogs', 'noLargeDogs', 'noFemaleDogs', 'noMaleDogs',
            'oKForSeniors', 'hypoallergenic', 'goodInCar', 'leashtrained', 'cratetrained',
            'fetches', 'playsToys', 'swims', 'oKWithFarmAnimals', 'drools', 'apartment', 'lap',
            'noHeat', 'noCold', 'protective', 'escapes', 'predatory', 'hasAllergies', 'specialDiet',
            'ongoingMedical', 'hearingImpaired', 'sightImpaired', 'obedient', 'playful', 'timid',
            'skittish', 'independent', 'affectionate', 'eagerToPlease', 'intelligent',
            'eventtempered', 'gentle', 'goofy', 'messagePet'
        ];
        foreach ($boolFields as $field) {
            if (isset($this->$field)) {
                $this->$field = !$this->$field ? 'No' : 'Yes';
            }
        }

        return $this;
    }
}

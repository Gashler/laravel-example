<?php

namespace App\Models\V2;

use Illuminate\Database\Eloquent\Model;

class BulkExportJob extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'entity',
        'partner_id',
        'contactPhone',
        'country',
        'options',
        'filters',
        'exlude_software',
        'delta_ts',
        'until_ts',
        'delta_type',
        'status',
    ];

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'bulk_export_job_id';

    /**
     * Disable table timestamps
     */
    public $timestamps = false;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'filters' => 'object',
    ];
}

<?php

namespace App\Models\V2;

use App\Models\V2\BulkExportPet;

interface BulkExportPetInterface
{
    /**
     * Fix contact name
     *
     * @param object $pet
     * @return void
     */
    public function getContactNameAttribute(): string;

    /**
     * Fix contact name
     *
     * @return string
     */
    public function getContactPhoneAttribute(): string;

    /**
     * Fix video URL
     *
     * @return array
     */
    public function getVideoUrlsAttribute(): array;

    /**
     * Generate breed string
     *
     * @return string
     */
    public function getBreedAttribute(): string;

    /**
     * Generate photo structure
     *
     * @return string
     */
    public function getPicturesAttribute(): string;

    /**
     * Build image location string
     *
     * @param integer $id
     * @param string $ext
     * @return string
     */
    public function getImageLocation(int $id, string $ext): string;

    /**
     * Fix bool fields
     *
     * @return BulkExportPet
     */
    public function fixBoolFields(): BulkExportPet;
}

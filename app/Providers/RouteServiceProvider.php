<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiAuthV1Routes();

        $this->mapApiAuthV2Routes();

        //
    }

    /**
     * Define the V1 legacy API routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiAuthV1Routes()
    {
        Route::middleware(['apiAuthV1', 'api'])
            ->namespace($this->namespace)
            ->group(base_path('routes/v1/api.php'));
    }

    /**
     * Define the V2 legacy API routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiAuthV2Routes()
    {
        Route::middleware(['apiAuthV2', 'api'])
            ->namespace($this->namespace)
            ->group(base_path('routes/v2/api.php'));
    }
}

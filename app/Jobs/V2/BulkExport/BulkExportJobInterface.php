<?php

namespace App\Jobs\V2\BulkExport;

use App\Models\V2\BulkExportJob;

interface BulkExportJobInterface
{
    /**
     * Run a bulk export job
     *
     * @param BulkExportJob $bulkExportJob
     * @param integer $limit
     * @param boolean $verbose
     */
    public function __construct(
        BulkExportJob $bulkExportJob,
        int $limit = null,
        bool $verbose = false
    );

    /**
     * Create a new formatted phone property and delete the unformatted phone
     * properties on the shelter ojbect
     *
     * @param object $shelter
     * @return object
     */
    public function formatPhone(object $shelter): object;

    /**
     * Create a new formatted fax property and delete the unformatted fax
     * properties on the shelter ojbect
     *
     * @param object $shelter
     * @return object
     */
    public function formatFax(object $shelter): object;

    /**
     * Detect if the URL is a facebook URL or otherwise, and return the
     * appropriate field for either case.
     *
     * @param object $shelter
     * @return object
     */
    public function formatShelterUrl(object $shelter): object;
}

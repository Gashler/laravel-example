<?php

namespace App\Jobs\V2\BulkExport;

use App\Models\V2\BulkExportJob;

interface BulkExportPetsJobInterface
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(BulkExportJob $job, int $limit = null, bool $verbose = false);

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle();
}

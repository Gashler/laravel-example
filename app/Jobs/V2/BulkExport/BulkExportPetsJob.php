<?php

namespace App\Jobs\V2\BulkExport;

use App\Models\V2\BulkExportJob;
use App\Models\V2\BulkExportPet;
use App\Models\V1\Partner;
use App\Jobs\V2\BulkExport\BulkExportPetsJobInterface;
use App\Services\V2\BulkExportService;
use DB;
use Log;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BulkExportPetsJob implements BulkExportPetsJobInterface, ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $bulkExportJob;
    protected $limit;
    protected $verbose;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(BulkExportJob $bulkExportJob, int $limit = null, bool $verbose = false)
    {
        $this->bulkExportJob = $bulkExportJob;
        $this->limit = $limit;
        $this->verbose = $verbose;
        $this->bulkExportService = new BulkExportService(
            Partner::find($bulkExportJob->partner_id)->api_key
        );
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $partnerId = $this->bulkExportJob->partner_id;

        $query = BulkExportPet::select(
            DB::raw('pet_id                    AS "animalID"'),
            DB::raw('pets.shelter_id           AS "orgID"'),
            DB::raw('pet_name                  AS "name"'),
            DB::raw('shelter_reference_code    AS "rescueID"'),
            DB::raw('clan_name                 AS "species"'),
            DB::raw('prim_families.family_name AS "primaryBreed"'),
            DB::raw('sec_families.family_name  AS "secondaryBreed"'),
            DB::raw(
                "CASE
                    WHEN sex='f' THEN 'Female'
                    WHEN sex='m' THEN 'Male'
                    ELSE NULL
                END                            AS sex"
            ),
            'age',
            'description',
            DB::raw(
                "CASE
                    WHEN purebred_p='t' THEN 'No'
                    WHEN purebred_p='f' THEN 'Yes'
                    ELSE NULL
                END                            AS mixed"
            ),
            DB::raw('public_name               AS "color"'),
            DB::raw('range_name                AS "size"'),
            DB::raw('hair_length               AS "coatLength"'),
            DB::raw('special_needs_p           AS "specialNeeds"'),
            DB::raw('shots_current_p           AS "uptodate"'),
            DB::raw('housetrained_p            AS "housetrained"'),
            DB::raw('declawed_p                AS "declawed"'),
            DB::raw('good_with_kids_p          AS "kids"'),
            DB::raw('good_with_dogs_p          AS "dogs"'),
            DB::raw('good_with_cats_p          AS "cats"'),
            DB::raw('spayed_neutered_p         AS "altered"'),
            DB::raw('experienced_adopter_p     AS "ownerExperience"'),
            DB::raw(
                'ROUND(
                    date_part(
                        ' . "'epoch'" . ',
                        last_modified_ts
                    )
                )                              AS "lastUpdated"'
            ),
            DB::raw('pet_state                 AS "status"'),
            DB::raw('pattern                   AS "pattern"'),
            DB::raw('courtesy_listing_p        AS "courtesy"'),
            DB::raw('foster_p                  AS "needsFoster"'),
            DB::raw('found_p                   AS "found"'),
            DB::raw('found_date                AS "foundDate"'),
            DB::raw('found_postal_code         AS "foundZipcode"'),
            // Always return *something* for the animalLocation. Prefer the specific
            // location_postal_code and fall back to shelters addr_postal_code
            DB::raw(
                'COALESCE(
                    location_postal_code,
                    addr_postal_code
                )                              AS "animalLocation"'
            ),
            DB::raw(
                'ROUND(
                    date_part(
                        ' . "'epoch'" . ',
                        scheduled_euth_date
                    )
                )                              AS "killDate"'
            ),
            DB::raw('scheduled_euth_reason     AS "killReason"'),
            DB::raw('good_with_adults          AS "oKWithAdults"'),
            DB::raw('obedience_training        AS "obedienceTraining"'),
            DB::raw('exercise_needs            AS "exerciseNeeds"'),
            DB::raw('energy_level              AS "energyLevel"'),
            DB::raw('activity_level            AS "activityLevel"'),
            DB::raw('ear_type                  AS "earType"'),
            DB::raw('eye_color                 AS "eyeColor"'),
            DB::raw('tail_type                 AS "tailType"'),
            DB::raw('grooming_needs            AS "groomingNeeds"'),
            DB::raw('yard_required_p           AS "yardRequired"'),
            DB::raw('fence_type                AS "fence"'),
            DB::raw('shedding_amount           AS "shedding"'),
            DB::raw('new_people_behavior       AS "newPeople"'),
            DB::raw('vocalizations             AS "vocal"'),
            DB::raw('older_kids_only_p         AS "olderKidsOnly"'),
            DB::raw('bad_with_small_dogs_p     AS "noSmallDogs"'),
            DB::raw('bad_with_large_dogs_p     AS "noLargeDogs"'),
            DB::raw('bad_with_female_dogs_p    AS "noFemaleDogs"'),
            DB::raw('bad_with_male_dogs_p      AS "noMaleDogs"'),
            DB::raw('good_for_seniors_p        AS "oKForSeniors"'),
            DB::raw('hypoallergenic_p          AS "hypoallergenic"'),
            DB::raw('good_in_car_p             AS "goodInCar"'),
            DB::raw('leash_trained_p           AS "leashtrained"'),
            DB::raw('crate_trained_p           AS "cratetrained"'),
            DB::raw('fetches_p                 AS "fetches"'),
            DB::raw('plays_with_toys_p         AS "playsToys"'),
            DB::raw('swims_p                   AS "swims"'),
            DB::raw('good_with_farm_animals_p  AS "oKWithFarmAnimals"'),
            DB::raw('drools_p                  AS "drools"'),
            DB::raw('apartment_friendly_p      AS "apartment"'),
            DB::raw('lap_p                     AS "lap"'),
            DB::raw('heat_sensitive_p          AS "noHeat"'),
            DB::raw('cold_sensitive_p          AS "noCold"'),
            DB::raw('protective_p              AS "protective"'),
            DB::raw('escapes_p                 AS "escapes"'),
            DB::raw('predatory_p               AS "predatory"'),
            DB::raw('has_allergies_p           AS "hasAllergies"'),
            DB::raw('special_diet_p            AS "specialDiet"'),
            DB::raw('ongoing_medical_p         AS "ongoingMedical"'),
            DB::raw('hearing_impaired_p        AS "hearingImpaired"'),
            DB::raw('sight_impaired_p          AS "sightImpaired"'),
            DB::raw('obedient_p                AS "obedient"'),
            DB::raw('playful_p                 AS "playful"'),
            DB::raw('timid_p                   AS "timid"'),
            DB::raw('skittish_p                AS "skittish"'),
            DB::raw('independent_p             AS "independent"'),
            DB::raw('affectionate_p            AS "affectionate"'),
            DB::raw('eager_to_please_p         AS "eagerToPlease"'),
            DB::raw('intelligent_p             AS "intelligent"'),
            DB::raw('even_tempered_p           AS "eventempered"'),
            DB::raw('gentle_p                  AS "gentle"'),
            DB::raw('goofy_p                   AS "goofy"'),
            DB::raw('weight_current            AS "sizeCurrent"'),
            DB::raw('weight_unit               AS "sizeUOM"'),
            DB::raw('adoption_cost             AS "adoptionFee"'),
            DB::raw(
                'ROUND(
                    date_part(
                        ' . "'epoch'" . ',
                        media_last_updated_ts
                    )
                )                              AS "mediaLastUpdated"'
            ),
            DB::raw('message_pet_p             AS "messagePet"'),
            DB::raw('pet_contacts.email        AS "contactEmail"'),

            // These will need to get munged into contactName and contactPhone later
            DB::raw('pet_contacts.first_name          AS "pet_contact_first_name"'),
            DB::raw('pet_contacts.last_name           AS "pet_contact_last_name"'),
            DB::raw('pet_contacts.phone_area_code     AS "pet_contact_phone_area_code"'),
            DB::raw('pet_contacts.phone_number        AS "pet_contact_phone_number"'),
            DB::raw('pet_contacts.phone_extension     AS "pet_contact_phone_extension"'),

            // This will need to get converted into a videoUrls structure later
            'youtube_video_id'
        )
            ->join('shelters_active', 'pets.shelter_id', '=', 'shelters_active.shelter_id')
            ->join(
                'zip_and_postal_codes',
                'shelters_active.postal_code_for_joining',
                '=',
                'zip_and_postal_codes.postal_code'
            )
            ->join('clans', 'pets.clan_id', 'clans.clan_id')
            ->leftJoin('pet_colors', 'pets.color_id', '=', 'pet_colors.pet_color_id')
            ->leftJoin(
                DB::raw('families_and_nicks_active AS "prim_families"'),
                DB::raw(
                    'COALESCE(
                    pets.primary_family_nick_id,
                    pets.primary_family_id
                )'
                ),
                '=',
                'prim_families.family_id'
            )
            ->leftJoin(
                'families_and_nicks_active AS sec_families',
                DB::raw(
                    'COALESCE(
                    pets.secondary_family_nick_id,
                    pets.secondary_family_id
                )'
                ),
                '=',
                'sec_families.family_id'
            )
            ->leftJoin(
                'pet_size_ranges',
                'pets.pet_size_range_id',
                'pet_size_ranges.pet_size_range_id'
            )
            ->leftJoin(
                'pet_contacts',
                'pets.pet_contact_id',
                '=',
                'pet_contacts.pet_contact_id'
            )

            // This, along with the "shelter_partner_optout_map.shelter_id IS NULL" WHERE clause
            // weeds out shelters who opted out
            ->leftJoin(
                'shelter_partner_optout_map',
                function ($join) use ($partnerId) {
                    $join->on(
                        'shelters_active.shelter_id',
                        '=',
                        'shelter_partner_optout_map.shelter_id'
                    );
                    $join->on('partner_id', '=', DB::raw("'" . $partnerId . "'"));
                }
            )
            ->whereIn('addr_country_code', explode('&', $this->bulkExportJob->country))
            ->whereNull('shelter_partner_optout_map.shelter_id');

        // For some reason the `casts` array on the BulkExportJob model isn't fully decoding the
        // JSON for the filters property
        $this->bulkExportJob->filters = json_decode($this->bulkExportJob->filters);
        if (
            isset($this->bulkExportJob->filters)
            && count($this->bulkExportJob->filters->shelter_id)
        ) {
            $query->whereIn('pets.shelter_id', $this->bulkExportJob->filters->shelter_id);
        }

        // HB-322, HB-328 Quick hack for disaster-recovery apps for Hurricane Harvey & Irma
        if ($partnerId === 559 || $partnerId === 563) {
            $query = $query->whereIn('shelters_active.addr_state_code', ['TX', 'LA', 'FL', 'PR']);
        }

        if (isset($this->bulkExportJob->exclude_software)) {
            $ssvId = DB::table('shelter_software_vendors')
                ->where('software_name', $this->bulkExportJob->exclude_software)
                ->get('ssv_id', 'ssv_id_name')
                ->pluck('ssv_id')
                ->first();

            // XXX Handle no ssv_id case. This, along with the "shelter_ssv_map.shelter_id IS NULL"
            // WHERE clause weeds out shelters from a particular SSV
            $query = $query->leftJoin(
                'shelter_ssv_map',
                function ($join) use ($ssvId) {
                    $join->on('shelters_active.shelter_id', '=', 'shelter_ssv_map.shelter_id');
                    $join->on('shelter_ssv_map.active_p', '=', DB::raw("'" . 1 . "'"));
                    $join->on('shelter_ssv_map.ssv_id', '=', DB::raw("'" . $ssvId . "'"));
                }
            )
                ->whereNull('shelter_ssv_map.shelter_id');
        }

        // In most cases we'll want available pets--except for the delta deleted case
        if (isset($this->bulkExportJob->delta_type) && isset($this->bulkExportJob->delta_ts)) {
            $untilTimestamp = $this->bulkExportJob->until_ts || time();
            if ($this->bulkExportJob->delta_type === 'deleted') {
                // Set the pet_state to deleted, and also check for any shelters that have newly
                // opted out of this partner. See FR-272.
                $query = $query->where(
                    function ($query) use ($untilTimestamp) {
                        return $query->where(
                            function ($query2) use ($untilTimestamp) {
                                return $query2
                                    ->where('pet_state', 'deleted')
                                    ->whereBetween(
                                        'pets.deleted_ts',
                                        [
                                            date('Y-m-d h:i:s', $this->bulkExportJob->delta_ts),
                                            date('Y-m-d h:i:s', $untilTimestamp)
                                        ]
                                    );
                            }
                        )->orWhere(
                            function ($query2) use ($untilTimestamp) {
                                return $query2
                                    ->where('pet_state', 'hidden')
                                    ->whereBetween(
                                        'pets.hidden_ts',
                                        [
                                            date('Y-m-d h:i:s', $this->bulkExportJob->delta_ts),
                                            date('Y-m-d h:i:s', $untilTimestamp)
                                        ]
                                    );
                            }
                        )->orWhere(
                            function ($query2) use ($untilTimestamp) {
                                return $query2
                                    ->where('pet_state', 'hidden')
                                    ->whereBetween(
                                        'pets.hidden_ts',
                                        [
                                            date('Y-m-d h:i:s', $this->bulkExportJob->delta_ts),
                                            date('Y-m-d h:i:s', $untilTimestamp)
                                        ]
                                    );
                            }
                        )->orWhere(
                            function ($query2) use ($untilTimestamp) {
                                return $query2
                                    ->where('pet_state', 'adopted')
                                    ->whereBetween(
                                        'pets.adopted_timestamp',
                                        [
                                            date('Y-m-d h:i:s', $this->bulkExportJob->delta_ts),
                                            date('Y-m-d h:i:s', $untilTimestamp)
                                        ]
                                    );
                            }
                        );
                    }
                )
                    ->orWhereIn(
                        'pets.shelter_id',
                        DB::table('shelter_partner_optout_map')
                            ->select('shelter_id')
                            ->where(
                                function ($query) use ($untilTimestamp) {
                                    return $query
                                        ->where('partner_id', $this->bulkExportJob->partner_id)
                                        ->whereBetween(
                                            'optout_ts',
                                            [
                                                date('Y-m-d h:i:s', $this->bulkExportJob->delta_ts),
                                                date('Y-m-d h:i:s', $untilTimestamp)
                                            ]
                                        );
                                }
                            )
                            ->get()
                            ->pluck('shelter_id')
                            ->toArray()
                    );
            } else {
                $colNames = [
                    'added'   => 'uploaded_timestamp',
                    'updated' => 'last_modified_ts',
                ];
                $colName = $colNames[$this->bulkExportJob->delta_type];
                $query = $query
                    ->where('pet_state', 'available')
                    ->whereBetween(
                        "pets.$colName",
                        [
                            date('Y-m-d h:i:s', $this->bulkExportJob->delta_ts),
                            date('Y-m-d h:i:s', $untilTimestamp)
                        ]
                    );
            }
        }

        $startTime = time();

        if ($this->verbose) {
            Log::info("SQL Statement to run: " . $query->toSql());
        }

        $pets = $query
            ->limit($this->limit)
            ->get()
            ->map(
                function (BulkExportPet $pet) {
                    if ($this->bulkExportJob->options === 'detailsUrl') {
                        $pet->detailsUrl = config('company.base_url') . "/pets/$pet->animalID";
                    }

                    return $pet->fixBoolFields();
                }
            );
        $recordCount = count($pets);

        if ($this->verbose) {
            Log::info("SQL query return number of pets: $recordCount");
        }

        $finishTime = time();
        $duration = $finishTime - $startTime;

        if ($this->verbose) {
            Log::info("Post-processed all pets, returning json. Total duration $duration seconds.");
        }

        return $pets;
    }
}

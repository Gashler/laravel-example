<?php

namespace App\Jobs\V2\BulkExport;

use App\Models\V2\BulkExportJob;
use App\Models\V1\Partner;
use App\Jobs\V2\BulkExport\BulkExportJobInterface;
use App\Services\V2\BulkExportService;
use DB;
use Log;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class BulkExportSheltersJob implements ShouldQueue, BulkExportJobInterface
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $bulkExportJob;
    protected $bulkExportService;
    protected $limit;
    protected $verbose;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(BulkExportJob $bulkExportJob, int $limit = null, bool $verbose = false)
    {
        $this->bulkExportJob = $bulkExportJob;
        $this->limit = $limit;
        $this->verbose = $verbose;
        $this->bulkExportService = new BulkExportService(
            Partner::find($bulkExportJob->partner_id)->api_key
        );
    }

    /**
     * Export shelters
     *
     * @return Collection
     */
    public function handle()
    {
        // Update status of job
        $this->bulkExportService->setStatus($this->bulkExportJob->bulk_export_job_id, 'started');

        // Log beginning of job
        if ($this->verbose) {
            Log::info("_create_shelter_export started with params: ", func_get_args());
        }

        // NOT shelters or shelters_active, because we have a deleted delta that lets us return
        // deleted shelters
        $partnerId = $this->bulkExportJob->partner_id;
        $query = DB::table('shelters_with_deleted')
            // This, along with the "shelter_partner_optout_map.shelter_id IS NULL" WHERE clause
            // weeds out shelters who opted out
            ->leftJoin(
                'shelter_partner_optout_map',
                function ($join) use ($partnerId) {
                    $join->on('shelters_with_deleted.shelter_id', '=', 'shelter_partner_optout_map.shelter_id');
                    $join->on('shelter_partner_optout_map.partner_id', '=', DB::raw("'" . $partnerId . "'"));
                }
            )
            ->whereIn('addr_country_code', explode('&', $this->bulkExportJob->country))
            ->whereNull('shelter_partner_optout_map.shelter_id');

        // HB-322, HB-328 Quick hack for disaster-recovery apps for Hurricane Harvey & Irma
        if ($partnerId === 559 || $partnerId === 563) {
            $query = $query->whereIn('addr_state_code', ['TX', 'LA', 'FL', 'PR']);
        }

        // In most cases we'll want active shelters--except for the delta deleted case
        $query = $query->where('shelter_state', 'active');

        if (isset($this->bulkExportJob->exclude_software)) {
            $ssvId = DB::table('shelter_software_vendors')
                ->where('software_name', $this->bulkExportJob->exclude_software)
                ->get('ssv_id', 'ssv_id_name')
                ->pluck('ssv_id')
                ->first();

            // XXX Handle no ssv_id case. This, along with the "shelter_ssv_map.shelter_id IS NULL"
            // WHERE clause weeds out shelters from a particular SSV
            $query = $query->leftJoin(
                'shelter_ssv_map',
                function ($join) use ($ssvId) {
                    $join->on('shelters_with_deleted.shelter_id', '=', 'shelter_ssv_map.shelter_id');
                    $join->on('shelter_ssv_map.active_p', '=', DB::raw("'" . 1 . "'"));
                    $join->on('shelter_ssv_map.ssv_id', '=', DB::raw("'" . $ssvId . "'"));
                }
            )
                ->whereNull('shelter_ssv_map.shelter_id');
        }

        if (isset($this->bulkExportJob->delta_type) && isset($this->bulkExportJob->delta_ts)) {
            $cols = [
                'all' => 'activated_timestamp',
                'added' => 'activated_timestamp',
                'updated' => 'last_updated_timestamp',
                'deleted' => 'deleted_ts',
            ];
            $colName = $cols[$this->bulkExportJob->delta_type];
            $query = $query->where("shelters_with_deleted.$colName", '>=', date('Y-m-d h:i:s', $this->bulkExportJob->delta_ts));

            if ($this->bulkExportJob->delta_type === 'deleted') {
                // Set the shelter_state to deleted, and also check for any shelters that have newly
                // opted out of this partner. See FR-272.
                // XXX Shouldn't this include "suspended" shelters?
                $optedOutShelterIds = DB::table('shelter_partner_optout_map')
                    ->where('partner_id', $partnerId)
                    ->where('optout_ts', '>=', date('Y-m-d h:i:s', $this->bulkExportJob->delta_ts))
                    ->get('shelter_id')
                    ->pluck('shelter_id')
                    ->toArray();
                $query = $query->where(
                    function ($query) use ($optedOutShelterIds) {
                        return $query->where('shelter_state', 'deleted')
                            ->orWhereIn('shelters_with_deleted.shelter_id', $optedOutShelterIds);
                    }
                );
            }
        }

        $query = $query->select(
            DB::raw('shelters_with_deleted.shelter_id AS orgID'),
            DB::raw('shelter_name AS name'),
            DB::raw('addr_line_1 AS address'),
            DB::raw('addr_city AS city'),
            DB::raw('addr_state_code AS state'),
            DB::raw('addr_postal_code AS zip'),
            DB::raw("
                (CASE
                    WHEN shelter_state='active' THEN 'Available'
                    WHEN shelter_state='deleted' THEN 'Deleted'
                    ELSE NULL
                END) AS status
            "),
            DB::raw("
                (CASE
                    WHEN addr_country_code='US' THEN 'United States'
                    WHEN addr_country_code='CA' THEN 'Canada'
                    ELSE NULL
                END) AS country
            "),
            DB::raw("
                (CASE
                    WHEN rescue_group_p='t' THEN 'Rescue'
                    WHEN rescue_group_p='f' THEN 'Shelter'
                    ELSE NULL
                END) AS orgType
            "),
            DB::raw('email AS email'),
            DB::raw('areas_served AS serveAreas'),
            DB::raw('shelter_adoption_process AS adoptionProcess'),
            DB::raw('shelter_desc AS about'),
            DB::raw('shelter_driving_dir AS meetPets'),

            // We don't have fields that track this info, so just return null for these
            DB::raw('NULL AS orgSpecies'),
            DB::raw('NULL AS services'),
            DB::raw('NULL AS allowAppSubmissions'),
            DB::raw('NULL AS messageOrg'),

            // These will need to get combined into "phone"
            'phone_area_code',
            'phone_number',
            'phone_extension',

            // These will need to get combined into "fax"
            'fax_area_code',
            'fax_number',

            // This will need to get split into "orgUrl" or "facebookUrl"
            'website_url',
        );

        // Execute query and map attributes
        $startTime = time();
        $results = $query
            ->limit($this->limit)
            ->get()
            ->map(
                function (object $shelter) {
                    $shelter = $this->formatPhone($shelter);
                    $shelter = $this->formatFax($shelter);
                    return $this->formatShelterUrl($shelter);
                }
            );

        // Log result count
        if ($this->verbose) {
            $endTime = time();
            $duration = $endTime - $startTime;
            Log::info("SQL query return number of shelters: " . count($results));
            Log::info("Post-processed all shelters. Total duration $duration seconds.");
        }

        // Update BulkExportJob record
        $this->bulkExportService->setStatus($this->bulkExportJob->bulk_export_job_id, 'completed');

        // TODO: (other tickets)

        // If zip archive was requested
        // return new BulkExportOutputZip(
        //     new BulkExportOutputJson($results)
        // );

        // // Otherwise just write file to JSON
        // return new BulkExportOutputJson($results);

        return $results;
    }

    /**
     * Create a new formatted phone property and delete the unformatted phone
     * properties on the shelter ojbect
     *
     * @param object $shelter
     * @return object
     */
    public function formatPhone($shelter): object
    {
        $shelter->phone = null;

        if (isset($shelter->phone_area_code) && isset($shelter->phone_number)) {
            $shelter->phone = '(' . $shelter->phone_area_code . ') ' . $shelter->phone_number;
            $shelter->phone .= isset($shelter->phone_extension) ? "x$shelter->phone_extension" : '';
        }

        unset($shelter->phone_area_code);
        unset($shelter->phone_number);
        unset($shelter->phone_extension);

        return $shelter;
    }

    /**
     * Create a new formatted fax property and delete the unformatted fax
     * properties on the shelter ojbect
     *
     * @param object $shelter
     * @return object
     */
    public function formatFax(object $shelter): object
    {
        $shelter->fax = null;

        if (isset($shelter->fax_area_code) && isset($shelter->fax_number)) {
            $shelter->fax = '(' . $shelter->fax_area_code . ') ' . $shelter->fax_number;
        }

        unset($shelter->fax_area_code);
        unset($shelter->fax_number);

        return $shelter;
    }

    /**
     * Detect if the URL is a facebook URL or otherwise, and return the
     * appropriate field for either case.
     *
     * @param object $shelter
     * @return object
     */
    public function formatShelterUrl(object $shelter): object
    {
        if (isset($shelter->website_url)) {
            if (strpos($shelter->website_url, 'facebook.com/') !== false) {
                $shelter->facebook_url = $shelter->website_url;
            } else {
                $shelter->org_url = $shelter->website_url;
            }
        }

        unset($shelter->website_url);

        return $shelter;
    }
}

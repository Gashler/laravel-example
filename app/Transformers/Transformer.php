<?php

namespace App\Transformers;

use App\Transformers\TransformerInterface;

abstract class Transformer implements TransformerInterface
{
    /**
     * Transform data
     *
     * @param string $key
     * @param object $data
     * @return array
     */
    public function transform($orientation = 'from', string $key, $rawData)
    {
        // get transform index (converting from dot notation)
        $array = explode('.', $key);
        $indexRoot = $array[0];
        unset($array[0]);
        $indexPath = implode('.', $array);
        $transformIndex = data_get($this->$indexRoot, $indexPath);

        // cycle through raw data and transform index
        foreach ($rawData as $rawDataKey => $rawObject) {
            $transformedObject = [];
            foreach ($rawObject as $rawObjectKey => $rawObjectValue) {
                foreach ($transformIndex as $transformKey => $transformValue) {
                    // store a backup of this variable against modifications
                    $rawObjectValueOriginal = $rawObjectValue;

                    /**
                     * Accommodate reuse of $transformKeys. The ~ symbols delineates the key from
                     * its number. Example usage:
                     *
                     *   'hair' => [
                     *       'hair_length' => 'label',
                     *       'hair_length~1' => 'value'
                     *   ]
                     */
                    $transformKey = explode('~', $transformKey)[0];

                    // call methods in transform index
                    if (is_array($transformValue)) {
                        $method = $transformValue['method'];
                        $rawObjectValue = $this->$method($rawObjectValue);
                        $transformValue = $transformValue['key'];
                    }

                    // perform transformations
                    if ($rawObjectKey === $transformKey) {
                        $transformedObject[$transformValue] = $rawObjectValue;
                    }

                    // reset any value modifications that were speicfic to this iteration
                    $rawObjectValue = $rawObjectValueOriginal;
                }
            }
            $transformedArray[] = $transformedObject;
        }

        return $transformedArray;
    }

    /**
     * Transform data for an exterior source
     *
     * @param string $key
     * @param object $data
     * @return @transform
     */
    public function to(string $key, $data)
    {
        return $this->transform('to', $key, $data);
    }

    /**
     * Capitalize value
     *
     * @param string $str
     * @return string $str
     */
    protected function ucfirst(string $str)
    {
        return ucfirst($str);
    }
}

<?php

/**
 * Definitions of how to transform data. The name of the variable designates the
 * output context. Array keys map the source's keys. Array values map the
 * destination's keys. They can be strings or arrays, which include
 */

namespace App\Transformers;

use App\Transformers\Transformer;

class PetTransformer extends Transformer
{
    protected $form = [
        'ages' => [
            'age' => [
                'key' => 'label',
                'method' => 'ucfirst'
            ],
            'age~1' => 'value'
        ],
        'breeds' => [
            'family_name_search' => 'label',
            'query_string_fmt' => 'value',
        ],
        'colors' => [
            'search_color_name' => 'label',
            'search_color_id' => 'value'
        ],
        'hair' => [
            'hair_length' => [
                'key' => 'label',
                'method' => 'ucfirst'
            ],
            'hair_length~1' => 'value'
        ]
    ];
}

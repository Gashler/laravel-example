<?php

namespace App\Transformers;

interface TransformerInterface
{
    public function to(string $key, $data);
}

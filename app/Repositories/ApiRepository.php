<?php

namespace App\Repositories;

use App\Repositories\ApiRepositoryInterface;
use Cache;

abstract class ApiRepository implements ApiRepositoryInterface
{
    protected $defaultTake;
    protected $model;

    /**
     * Execute complex queries
     *
     * @param array $params
     * @param Model $query
     * @return Illuminate\Support\Collection
     */
    public function query(array $params, $query)
    {
        $skip = $params['start_number'] ?? 1;
        $skip -= 1;
        $take = $params['end_number'] ?? config('api.take');
        $take -= $skip + 1;
        $max = $params['max_take'] ?? config('api.max_take');
        $query->skip($skip);
        $take += $skip;
        if ($take > $max) {
            $take = $max;
        }

        return $query
            ->skip($skip)
            ->take($take)
            ->get();
    }

    /**
     * Retrieve a listing of the resource.
     *
     * @param  array  $params
     * @return array
     */
    public function index(array $params, $query = null)
    {
        // build query
        $query = $this->model;
        if (isset($params['ids'])) {
            $query->whereIn($this->model->primary_key, $params['ids']);
        }
        if (isset($params['with'])) {
            $query->with($params['with']);
        }

        // excecute query
        return $this->query($params, $query);
    }

    /**
     * Display the specified resource.
     *
     * @param  integer  $id
     * @return  object
     */
    public function show(int $id, array $with = [], $params = [])
    {
        $cacheName = class_basename($this->model) . "_$id";
        return Cache::remember(
            $cacheName,
            config('api.cache_lifespan'),
            fn () => $this->model::with($with)->findOrFail($id)
        );
    }
}

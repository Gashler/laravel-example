<?php

namespace App\Repositories;

use App\Repositories\ApiRepository;
use App\Models\V1\Shelter;

class ShelterRepository extends ApiRepository
{
    public function __construct()
    {
        $this->model = new Shelter;
    }

    /**
     * Search for pets
     *
     * @param  array  $params
     * @return @query
     */
    public function search(array $params)
    {
        // TODO: handle geotargeting
        // TODO: handle mata_only

        // build query
        $query = $this->model;
        $params['adopts_out'] = $params['adopts_out'] ?? true;
        $query->where('adopts_out', $params['adopts_out']);

        // execute query
        return $this->query($params, $query);
    }
}

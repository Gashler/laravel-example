<?php

namespace App\Repositories;

interface ApiRepositoryInterface
{
    public function index(array $params);

    public function show(int $id, array $with, array $params);
}

<?php

namespace App\Repositories;

use App\Repositories\ApiRepository;
use App\Models\V1\Pet;
use App\Transformers\PetTransformer;
use Cache;
use DB;

class PetRepository extends ApiRepository implements PetRepositoryInterface
{
    protected $transform;

    public function __construct()
    {
        $this->model = new Pet;
        $this->transform = new PetTransformer;
    }

    /**
     * Search for pets
     *
     * @param  array  $params
     * @return @query
     */
    public function search(array $params)
    {
        // TODO: handle geotargeting
        // TODO: handle mata_only

        // build query
        $query = $this->model::where('clan_id', $params['clan_id']);
        if (isset($params['breed_id'])) {
            $query->where('primary_family_id', $params['breed_id']);
        }
        if (isset($params['sex'])) {
            $query->where('sex', $params['sex']);
        }
        if (isset($params['color_id'])) {
            $query->where('color_id', $params['color_id']);
        }
        if (isset($params['age'])) {
            $query->where('age', $params['age']);
        }
        if (isset($params['pet_size_range_id'])) {
            $query->where('pet_size_range_id', $params['pet_size_range_id']);
        }
        if (isset($params['hair'])) {
            $query->where('hair_length', $params['hair']);
        }
        if (isset($params['special_needs'])) {
            $query->where('special_needs_p', $params['special_needs']);
        }
        if (isset($params['bonded_pair'])) {
            $query->whereNotNull('bonded_to');
        }

        // TODO: handle include_mixes boolean (rabbit and horse only)

        // TODO: handle 'added_after'


        // execute query
        return $this->query($params, $query);
    }

    /**
     * Get the data required for fields in a pet search form
     *
     * @param int $clan_id
     * @return mixed
     */
    public function searchForm(int $clanId)
    {
        $array = [];

        // get age
        $ages = Cache::remember(
            'pet.ages',
            config('api.cache_lifespan'),
            fn () =>
            DB::table('pet_catalog.ages')
                ->where('clan_id', $clanId)
                ->get()
        );
        $array['age'] = $this->transform->to('form.ages', $ages);
        array_unshift($array['age'], ['label' => 'Any', 'value' => '']);

        // get breed_id
        $breeds = Cache::remember(
            'pet.breeds',
            config('api.cache_lifespan'),
            fn () =>
            DB::table('families_all_real_and_virtual_active')
                ->where('clan_id', $clanId)
                ->get()
        );
        $array['breed_id'] = $this->transform->to('form.breeds', $breeds);
        array_unshift($array['breed_id'], ['label' => 'Any', 'value' => '']);

        // get color_id
        $colors = Cache::remember(
            'pet.colors',
            config('api.cache_lifespan'),
            fn () =>
            DB::table('pet_catalog.pet_color_search_names')
                ->where('clan_id', $clanId)
                ->get()
        );
        $array['color_id'] = $this->transform->to('form.colors', $colors);
        array_unshift($array['color_id'], ['label' => 'Any', 'value' => '']);

        // get geo_range
        $array['geo_range'] = config('geo-ranges');

        // get hair
        $hair = Cache::remember(
            'pet.hair',
            config('api.cache_lifespan'),
            fn () =>
            DB::table('pet_catalog.hair')
                ->where('clan_id', $clanId)
                ->get()
        );
        $array['hair'] = $this->transform->to('form.hair', $hair);
        array_unshift($array['hair'], ['label' => 'Any', 'value' => '']);

        // get sex
        $array['sex'] = config('sexes');

        return $array;
    }

    /**
     * Get the available breed IDs for the given clan ID
     *
     * @param int $clanId
     * @return array $breedIds
     */
    public function getBreedIds(int $clanId)
    {
        return DB::table('pet_catalog.families')
            ->where('clan_id', $clanId)
            ->pluck('family_id')
            ->toArray();
    }

    /**
     * Get the available color IDs for the given clan ID
     *
     * @param int $clanId
     * @return array $colorIds
     */
    public function getColorIds(int $clanId)
    {
        return DB::table('pet_catalog.pet_color_search_names')
            ->where('clan_id', $clanId)
            ->pluck('search_color_id')
            ->toArray();
    }

    /**
     * Get the available pet size range IDs for the given clan ID
     *
     * @param int $clanId
     * @return array $breedIds
     */
    public function getSizeRangeIds(int $clanId)
    {
        return DB::table('pet_catalog.vw_pet_size_ranges')
            ->where('clan_id', $clanId)
            ->pluck('pet_size_range_id')
            ->toArray();
    }
}

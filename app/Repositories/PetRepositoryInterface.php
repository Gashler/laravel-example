<?php

namespace App\Repositories;

interface PetRepositoryInterface
{
    public function search(array $params);

    public function searchForm(int $clanId);

    public function getBreedIds(int $clanId);

    public function getColorIds(int $clanId);

    public function getSizeRangeIds(int $clanId);
}

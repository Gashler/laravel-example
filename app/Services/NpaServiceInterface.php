<?php

namespace App\Services;

interface NpaServiceInterface
{
    public function subscribe(string $email, array $searchParams);
}

<?php

namespace App\Services;

use App\Exceptions\NpaSubscribeException;
use App\Services\NpaServiceInterface;
use DB;

class NpaService implements NpaServiceInterface
{
    /**
     * Subscribe a user to NPA
     *
     * @param string $email
     * @param string $search
     * @return boolean
     */
    public function subscribe(string $email, array $search)
    {
        // Arrange search params in the order and format expected by the
        // database function
        $zip = $search['postal_code'] ?? $search['city_state'];
        $args = [
            $email,
            $search['age'] ?? null,
            $search['bonded_pair'] ?? null,
            $search['breed'] ?? null,
            $search['breed_id'] ?? null,
            $search['clan'] ?? null,
            $search['clan_id'] ?? null,
            $search['color'] ?? null,
            $search['color_id'] ?? null,
            $search['geo_range'] ?? null,
            $search['hair'] ?? null,
            $search['sex'] ?? null,
            $search['size_id'] ?? null,
            $search['size_range'] ?? null,
            $search['special_needs'] ?? null,
            $zip,
            $search['include_mixes'] ?? null,
            $search['source'] ?? null,
            $search['partner_id'] ?? null,
        ];
        try {
            $result = DB::connection('pgsql_write')->select('SELECT marketing.post_new_pet_alert(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', $args);
        } catch (\Exception $e) {
            if ($e->getCode() === 'P0001') {
                throw new NpaSubscribeException();
            }
        }
        return $result[0];
    }
}

<?php

namespace App\Services\V2;

use App\Exceptions\V2\BulkExport\BulkExportDownloadNotFound;
use App\Exceptions\V2\BulkExport\BulkExportDownloadNotReady;
use App\Exceptions\V2\BulkExport\BulkExportStatusNotFound;
use App\Jobs\V2\BulkExport\BulkExportPetsJob;
use App\Jobs\V2\BulkExport\BulkExportSheltersJob;
use App\Models\V1\Partner;
use App\Services\V2\BulkExportServiceInterface;
use App\Models\V2\BulkExportJob;
use DB;
use Exception;

class BulkExportService implements BulkExportServiceInterface
{
    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
        $this->partnerId = Partner::where('api_key', $apiKey)
            ->first()
            ->partner_id;
    }

    /**
     * Handle pets export
     */
    public function handlePetsExport(array $params): array
    {
        // Initialize variablers
        $params['org_ids'] = $params['org_ids'] ?? [];
        $params['country'] = $params['country'] ?? 'US&CA';
        $params['delta_type'] = $params['delta_type'] ?? null;
        $params['delta_type'] = 'all_pets' ? null : $params['delta_type'];
        $params['options'] = $params['options'] ?? null;
        $params['exclude_software'] = $params['exclude_software'] ?? null;
        $params['since_ts'] = $params['since_ts'] ?? null;
        $params['until_ts'] = $params['until_ts'] ?? null;
        $params['limit'] = $params['limit'] ?? null;
        $params['verbose'] = $params['verbose'] ?? false;

        // Note: status, status_token, download_token, job_pending_ts will all
        // be populatd automatically by defaults in the SQL
        $job = [
            'entity' => 'pets',
            'partner_id' => $this->partnerId,
            'country' => $params['country'],
            'options' => $params['options'],
            'filters' => json_encode(['shelter_id' => $params['org_ids']]),
            'exclude_software' => $params['exclude_software'],
            'delta_ts' => $params['since_ts'],
            'until_ts' => $params['until_ts'],
            'delta_type' => $params['delta_type'],
        ];

        // Create record for job
        $bulkExportJob = BulkExportJob::create($job)->fresh();

        // Handle export job
        BulkExportPetsJob::dispatch($bulkExportJob, $params['limit'], $params['verbose']);

        // Create status URL for job
        $statusUrl = url('/') . "/bulk-export/v2/" . $params['delta_type'];
        $statusUrl .= "/status?key=$this->apiKey&status_token=$bulkExportJob->status_token";

        return [
            'location' => $statusUrl,
            'status_token' => $bulkExportJob->status_token,
        ];
    }

    /**
     * Handle shelter export
     */
    public function handleSheltersExport(array $params): array
    {
        // Initialize variablers
        $params['country'] = $params['country'] ?? 'US&CA';
        $params['exclude_software'] = $params['exclude_software'] ?? null;
        $params['since_ts'] = $params['since_ts'] ?? null;
        $params['until_ts'] = $params['until_ts'] ?? null;
        $params['delta_type'] = $params['delta_type'] ?? null;
        $params['delta_type'] = 'all_shelters' ? null : $params['delta_type'];
        $params['limit'] = $params['limit'] ?? null;
        $params['verbose'] = $params['verbose'] ?? false;
        $job = [
            'entity' => 'shelters',
            'partner_id' => $this->partnerId,
            'country' => $params['country'],
            'exclude_software' => $params['exclude_software'],
            'delta_ts' => $params['since_ts'],
            'until_ts' => $params['until_ts'],
            'delta_type' => $params['delta_type'],
        ];

        // Create record for job
        $bulkExportJob = BulkExportJob::create($job)->fresh();

        // Handle export job
        BulkExportSheltersJob::dispatch($bulkExportJob, $params['limit'], $params['verbose']);

        // Create status URL for job
        $statusUrl = url('/') . "/bulk-export/v2/" . $params['delta_type'];
        $statusUrl .= "/status?key=$this->apiKey&status_token=$bulkExportJob->status_token";

        return [
            'location' => $statusUrl,
            'status_token' => $bulkExportJob->status_token,
        ];
    }

    /**
     * Get status of bulk export
     */
    public function getStatus(string $statusToken): BulkExportJob
    {
        try {
            $bulkExportJob = BulkExportJob::where(
                [
                    'status_token' => $statusToken,
                    'partner_id' => $this->partnerId
                ]
            )->firstOrFail();
        } catch (Exception $e) {
            throw new BulkExportStatusNotFound;
        }

        // Create download URL jor job
        $resource = !isset($bulkExportJob->delta_type) ? 'all_' . $bulkExportJob->entity
            : $bulkExportJob->entity . '_' . $bulkExportJob->delta_type;
        $url = config('company.base_url') . "/bulk-export/v2/$resource/download?key=$this->apiKey";
        $url .= "&download_token=$bulkExportJob->download_token";
        $bulkExportJob->download_url = $url;

        return $bulkExportJob;
    }

    /**
     * Set status of a bulk export
     */
    public function setStatus(int $bulkExportJobId, string $status): bool
    {
        return BulkExportJob::findOrFail($bulkExportJobId)
            ->update(
                [
                    'status' => $status,
                    'job_started_ts' => DB::raw('CURRENT_TIMESTAMP')
                ]
            );
    }

    /**
     * Return bulk export URL
     */
    public function download(string $downloadToken): string
    {
        try {
            $bulkExportJob = BulkExportJob::where(
                [
                    'partner_id' => $this->partnerId,
                    'download_token' => $downloadToken
                ]
            )->firstOrFail();
        } catch (Exception $e) {
            throw new BulkExportDownloadNotFound;
        }

        // Check that job status is not marked as completed
        if ($bulkExportJob->status !== 'completed') {
            throw new BulkExportDownloadNotReady;
        }

        // Create download path
        $downloadUrl = config('api.download_url') . "$bulkExportJob->download_token.json";

        return $downloadUrl;
    }
}

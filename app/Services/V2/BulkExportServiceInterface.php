<?php

namespace App\Services\V2;

use App\Models\V2\BulkExportJob;

interface BulkExportServiceInterface
{
    /**
     * Begin the processes for exporting pets
     *
     * @param array $params
     * @return array
     */
    public function handlePetsExport(array $params): array;

    /**
     * Begin the processes for exporting shelters
     *
     * @param array $params
     * @return array
     */
    public function handleSheltersExport(array $params): array;

    /**
     * Check the status of a job
     *
     * @param string $statusToken
     * @return BulkExportJob
     */
    public function getStatus(string $statusToken): BulkExportJob;

    /**
     * Set the status of a job
     *
     * @param integer $bulkExportJobId
     * @param string $status
     * @return boolean
     */
    public function setStatus(int $bulkExportJobId, string $status): bool;

    /**
     * Return the download URL for a completed job
     *
     * @param string $downloadToken
     * @return string
     */
    public function download(string $downloadToken): string;
}

<?php

namespace App\Services\V1;

use DB;

interface BulkExportServiceInterface
{
    public function pets(array $countryCodes, int $limit);

    public function shelters(
        array $countryCodes,
        string $addedAfterTimestamp = null,
        int $limit
    );
}

<?php

namespace App\Services\V1;

use App\Models\V1\BulkExportPet;
use App\Models\V1\BulkExportShelter;
use App\Models\V1\Partner;
use App\Services\V1\BulkExportServiceInterface;
use Illuminate\Support\Collection;
use DB;

class BulkExportService implements BulkExportServiceInterface
{
    public function __construct(string $apiKey)
    {
        $this->petHarborId = Partner::where('tech_name', 'pet_harbor')
            ->pluck('partner_id')
            ->first();

        $this->partnerId = Partner::where('api_key', $apiKey)
            ->pluck('partner_id')
            ->first();

        $this->chameleonSheltersToExclude = DB::table('shelter_ssv_map')
            ->join('shelter_software_vendors', 'shelter_software_vendors.ssv_id', '=', 'shelter_ssv_map.ssv_id')
            ->where('shelter_ssv_map.active_p', true)
            ->where('shelter_software_vendors.active_p', true)
            ->where('shelter_software_vendors.software_name', 'like', 'chameleon%')
            ->get('shelter_id')
            ->pluck('shelter_id')
            ->toArray();
    }

    /**
     * Export pets
     *
     * @param array $countryCodes
     * @param int $limit
     * @return Collection
     */
    public function pets(array $countryCodes, int $limit = null): Collection
    {
        // prepare inputs
        $photoUrlBase = config('company.base_url') . '/cgi-bin/public/petsearch.cgi/photo_url?pet_id=';
        $status = 'available';

        // build query
        $query = BulkExportPet::where('pets.pet_state', $status)
            ->whereIn('shelters_active.addr_country_code', $countryCodes)
            ->whereNull('shelter_partner_optout_map.shelter_id')
            ->join('shelters_active', 'shelters_active.shelter_id', '=', 'pets.shelter_id')
            ->join('zip_and_postal_codes', 'shelters_active.postal_code_for_joining', '=', 'zip_and_postal_codes.postal_code')
            ->join('clans', 'clans.clan_id', '=', 'pets.clan_id')
            ->leftJoin('photos', 'pets.photo_1a_id', '=', 'photos.photo_id')
            ->leftJoin('pet_colors', 'pets.color_id', '=', 'pet_colors.pet_color_id')
            ->leftJoin('families_and_nicks_active as prim_families', DB::raw('COALESCE(pets.primary_family_nick_id,pets.primary_family_id)'), '=', 'prim_families.family_id')
            ->leftJoin('families_and_nicks_active as sec_families', DB::raw('COALESCE(pets.secondary_family_nick_id,pets.secondary_family_id)'), '=', 'sec_families.family_id')
            ->leftJoin('pet_size_ranges', 'pets.pet_size_range_id', '=', 'pet_size_ranges.pet_size_range_id')
            ->leftJoin(
                'shelter_partner_optout_map',
                function ($join) {
                    $join->on('shelters_active.shelter_id', '=', 'shelter_partner_optout_map.shelter_id');
                    $join->on('shelter_partner_optout_map.partner_id', '=', DB::raw("'" . $this->partnerId . "'"));
                }
            )
            ->select(
                'pet_id',
                'pets.shelter_id',
                'pet_name',
                'shelter_reference_code',
                DB::raw('clan_name AS species'),
                DB::raw('prim_families.family_name AS primary_breed'),
                DB::raw('sec_families.family_name AS secondary_breed'),
                DB::raw('public_name AS color'),
                'sex',
                'age',
                DB::raw('range_name AS size'),
                'hair_length',
                DB::raw("'" . $photoUrlBase . "' || pet_id || '&clan_id=' || pets.clan_id AS photo_url"),
                'description',
                'special_needs_p',
                'purebred_p',
                'shots_current_p',
                'housetrained_p',
                'declawed_p',
                'good_with_kids_p',
                'good_with_dogs_p',
                'good_with_cats_p',
                'spayed_neutered_p',
                DB::raw("upper(to_char(pets.uploaded_timestamp, 'YYYY-MM-DDtHH24:MI:SS')) as pet_uploaded_timestamp"),
                DB::raw("upper(to_char(photos.uploaded_timestamp, 'YYYY-MM-DDtHH24:MI:SS')) as photo_uploaded_timestamp"),
                'shelters_active.shelter_name',
                'addr_country_code',
                'addr_state_code',
                'addr_city',
                'addr_postal_code',
                DB::raw('latitude(earth_coords) as latitude'),
                DB::raw('longitude(earth_coords) as longitude'),
                DB::raw('pets.clan_id AS clan_id')
            );

        // If PetHarbor is calling this, perform an additional shelter exclusion
        if ($this->partnerId === $this->petHarborId) {
            $query = $query->whereNotIn('pets.shelter_id', $this->chameleonSheltersToExclude);
        }

        // If a limit is set (for testing)
        if (isset($limit)) {
            $query = $query->limit($limit);
        }

        // Execute query and map pet_details_url attributes
        $pets = $query
            ->get()
            ->map(
                function ($pet) {
                    $pet->pet_details_url = config('company.base_url') . '/pets/' . $pet->pet_id;
                    return $pet;
                }
            );

        return $pets;
    }

    /**
     * Export shelters
     *
     * @param array $countryCodes
     * @param string $addedAfterTimestamp
     * @param int $limit
     * @return Collection
     */
    public function shelters(
        array $countryCodes,
        string $addedAfterTimestamp = null,
        int $limit = null
    ): Collection {
        $query = BulkExportShelter::select(
            'shelters_active.shelter_id',
            'shelter_name',
            'website_url',
            'email',
            'phone_area_code',
            'phone_number',
            'phone_extension',
            'fax_area_code',
            'fax_number',
            'addr_line_1',
            'addr_line_2',
            'addr_city',
            'addr_state_code',
            'addr_postal_code',
            'addr_country_code',
            'shelter_desc',
            'shelter_adoption_process',
            'shelter_driving_dir',
            'adopts_out_dogs_p',
            'adopts_out_cats_p',
            'rescue_group_p',
            DB::raw("upper(to_char(last_pet_added_timestamp, 'YYYY-MM-DDtHH24:MI:SS')) as last_pet_added_timestamp"),
            DB::raw("upper(to_char(last_pet_updated_timestamp, 'YYYY-MM-DDtHH24:MI:SS')) as last_pet_updated_timestamp"),
            DB::raw("upper(to_char(activated_timestamp, 'YYYY-MM-DDtHH24:MI:SS')) as added_timestamp"),
            DB::raw("coalesce(donation_url, donation_html) as donation_html")
        )

            // This, along with the IS NULL condition below is how we weed out the shelters who opted-out
            ->leftJoin(
                'shelter_partner_optout_map',
                function ($join) {
                    $join->on('shelters_active.shelter_id', '=', 'shelter_partner_optout_map.shelter_id');
                    $join->on('partner_id', '=', DB::raw("'" . $this->partnerId . "'"));
                }
            )
            ->whereNull('shelter_partner_optout_map.shelter_id')
            ->whereIn('addr_country_code', $countryCodes)
            ->whereNotNull('shelters_active.activated_timestamp');

        // Limit date range of query
        if ($addedAfterTimestamp) {
            $query = $query->where('shelters_active.activated_timestamp', '>=', $addedAfterTimestamp);
        }

        // If PetHarbor is calling this, perform an additional shelter exclusion
        if ($this->partnerId === $this->petHarborId) {
            $query = $query
                ->whereNotIn('shelters_active.shelter_id', $this->chameleonSheltersToExclude);
        }

        return $query->get();
    }
}

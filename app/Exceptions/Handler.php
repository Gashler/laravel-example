<?php

namespace App\Exceptions;

use App\Http\Responses\V1\ApiResponse;
use App\Http\Responses\ResponseInterface;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * Adapt Laravel or generic exception classes. For custom exceptions,
     * these properties should be defined within exception classes.
     *
     * @var array
     */
    protected $exceptionMap = [
        'Illuminate\Database\Eloquent\ModelNotFoundException' => [
            'details' => 'No shelter matched the ID provided.',
            'statusCode' => 400,
            'msg' => "input_invalid"
        ],
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Get instance of ApiResponse
     *
     * @return ApiResponseInterface
     */
    public function responder(): ResponseInterface
    {
        return new ApiResponse;
    }

    /**
     * Report or log an exception.
     *
     * Note: There is an issue getting complete code coverage
     * for methods that return nothing and throw an exception.
     * This is because of the way PHP works, and is documented
     * to some extent by the folks working on PHPUnit. Because
     * of this, we explicitly ignore this method, however it is
     * being tested.
     *
     * @codeCoverageIgnore
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $exception->statusCode = 500;
        if (isset($this->exceptionMap[get_class($exception)])) {
            foreach ($this->exceptionMap[get_class($exception)] as $key => $value) {
                $exception->$key = $value;
            }
        }
        return $this->responder()->error($exception);

        // return parent::render($request, $exception);
    }
}

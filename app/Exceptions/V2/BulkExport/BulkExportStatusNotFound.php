<?php

namespace App\Exceptions\V2\BulkExport;

use Exception;

class BulkExportStatusNotFound extends Exception
{
    public $code = 404;

    public $details = "Invalid status_token. Please include the hexidecimal status_token from the response of your initial export request";

    public $message = 'input_invalid';
}

<?php

namespace App\Exceptions\V2\BulkExport;

use Exception;

class BulkExportDownloadNotReady extends Exception
{
    public $code = 500;

    public $details = "Export not yet complete. Use the status resource to check on the export's status.";

    public $message = 'resource_not_found';
}

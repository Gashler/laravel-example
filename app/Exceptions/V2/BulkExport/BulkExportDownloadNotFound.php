<?php

namespace App\Exceptions\V2\BulkExport;

use Exception;

class BulkExportDownloadNotFound extends Exception
{
    public $code = 404;

    public $details = "Invalid download_token. Please use the download_token returned from the status resource once the export has completed.";

    public $message = 'input_invalid';
}

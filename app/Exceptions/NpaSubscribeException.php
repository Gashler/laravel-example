<?php

namespace App\Exceptions;

class NpaSubscribeException extends \Exception
{
    public $code = 450;

    public $details = 'Email address and search already exists. New NPA subscribe not processed.';

    public $invalid = [
        'email',
        'search_params',
    ];

    public $msg = 'validation_failure';
}

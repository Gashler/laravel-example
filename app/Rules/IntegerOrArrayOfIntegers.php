<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IntegerOrArrayOfIntegers implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (is_int($value)) {
            return true;
        } elseif (is_array($value)) {
            foreach ($value as $subValue) {
                if (!is_int($subValue)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Must be an integer or an array of integers.';
    }
}

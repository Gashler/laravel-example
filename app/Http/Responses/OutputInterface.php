<?php

namespace App\Http\Responses;

use Symfony\Component\HttpFoundation\StreamedResponse;

interface OutputInterface
{
    /**
     * Format and return the output
     *
     * @param mixed $response
     * @param integer $statusCode
     * @return Response
     */
    public function output($response, int $statusCode);
}

<?php

namespace App\Http\Responses\V1;

use App\Http\Responses\Response;

class ApiResponse extends Response
{
    /**
     * Prepare successful responses
     *
     * @param $data
     * @return @output
     */
    public function handle($data)
    {
        $response = [
            'status' => 'ok',
            'body' => $data
        ];

        return $this->output($response, 200);
    }

    /**
     * Prepare error responses
     *
     * @param \Exception $e
     * @return @output
     */
    public function error(\Exception $e)
    {
        $response = [
            'status' => 'fail',
            'code' => $e->statusCode,
            'error' => [
                'details' => $e->details ?? null,
                'invalid' => $e->invalid ?? [],
                'missing' => $e->missing ?? [],
                'msg' => $e->msg ?? $e->getMessage(),
                'unknown' => $e->unknown ?? [],
            ],
        ];
        return $this->output($response, $e->statusCode);
    }
}

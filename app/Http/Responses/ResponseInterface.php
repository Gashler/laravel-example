<?php

namespace App\Http\Responses;

interface ResponseInterface
{
    /**
     * Instantiate the output class
     *
     * @param OutputInterface $formatter
     *
     * @return void
     */
    public function __construct(OutputInterface $formatter = null);

    /**
     * Prepare a responsse for a success state
     *
     * @param mixed $data - the response body
     * @param boolean $meta - whether or not to include response meta data
     *
     * @return @output
     */
    public function handle($data);

    /**
     * Format and return the response
     *
     * @param mixed $response
     *
     * @return App\Http\Responses\OutputInterface
     */
    public function output($response);
}

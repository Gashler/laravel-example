<?php

namespace App\Http\Responses\V2;

use App\Http\Responses\Response;

class BulkExportResponse extends Response
{
    /**
     * Prepare successful responses
     *
     * @param $data
     * @return @output
     */
    public function handle($data)
    {
        $base = ['status' => 'ok'];
        $response = array_merge($base, $data);

        return $this->output($response, 200);
    }
}

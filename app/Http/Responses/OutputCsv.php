<?php

namespace App\Http\Responses;

use Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

class OutputCsv implements OutputInterface
{
    /**
     * Format and return the output
     *
     * @param mixed $response
     * @param integer $statusCode
     * @return void
     */
    public function output($response, int $statusCode = 200): StreamedResponse
    {
        $headers = [
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename=' . time() . '.csv',
            'Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', time() + config('api.bulk_export_file_expiration_time')), // 1 hour
            'Pragma' => 'public'
        ];

        // Add headers for each column in the CSV download
        if (!is_array($response)) {
            $response = $response->toArray();
        }
        array_unshift($response, array_keys($response[0]));

        return Response::stream($this->callback($response), $statusCode, $headers);
    }

    public function callback($response)
    {
        if (!is_array($response)) {
            $response = $response->toArray();
        }
        $FH = fopen('php://output', 'w');
        foreach ($response as $row) {
            fputcsv($FH, $row);
        }
        fclose($FH);
    }
}

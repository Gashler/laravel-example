<?php

namespace App\Http\Responses;

class OutputJson implements OutputInterface
{
    public function output($response, int $statusCode)
    {
        return response()->json($response, $statusCode);
    }
}

<?php

namespace App\Http\Responses;

use App\Http\Responses\OutputInterface;
use App\Http\Responses\OutputJson;

class Response implements ResponseInterface
{
    protected $formatter;

    public function __construct(OutputInterface $formatter = null)
    {
        $formatter = $formatter ?? new OutputJson;
        $this->formatter = $formatter;
    }

    /**
     * Format and return the output
     *
     * @param array $response
     * @return OutputInterface
     */
    public function output($response, int $responseCode = 200)
    {
        return $this->formatter->output($response, $responseCode);
    }

    /**
     * Prepare successful responses
     *
     * @param $data
     * @return @output
     */
    public function handle($data)
    {
        return $this->output($data, 200);
    }
}

<?php

namespace App\Http\Requests\V1\Npa;

use App\Repositories\PetRepository;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class NpaSubscribeRequest extends FormRequest
{
    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    public function all($keys = null)
    {
        // get search_params and convert to array
        $inputs = parent::all();
        parse_str(str_replace(';', '&', $inputs['search_params']), $search);

        // if clan_id isn't provided, derive it from the species and vice versa
        $search['clan_id'] = config('clans')[$search['species']]['clan_id'];

        // apply changes
        $inputs['search_params'] = $search;
        return $inputs;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $inputs = $this->all();
        $this->petRepo = new PetRepository;

        return [
            'email' => 'required|email',
            'key' => 'required',
            'search_params' => 'required',
            'search_params.clan_id' => 'required',
            'search_params.geo_range' => 'required',
            'search_params.species' => 'required',
            'search_params.postal_code' => 'required_without:search_params.city_state',
            'search_params.city_state' => 'required_without:search_params.postal_code',
            'search_params.age' => [
                Rule::in(
                    config('ages')[$inputs['search_params']['clan_id']]
                ),
            ],
            'search_params.breed_id' => [
                Rule::in(
                    $this->petRepo->getBreedIds($inputs['search_params']['clan_id'])
                ),
            ],
            'search_params.color_id' => [
                Rule::in(
                    $this->petRepo->getColorIds($inputs['search_params']['clan_id'])
                ),
            ],
            'search_params.pet_size_range_id' => [
                Rule::in(
                    $this->petRepo->getSizeRangeIds($inputs['search_params']['clan_id'])
                ),
            ],
        ];
    }
}

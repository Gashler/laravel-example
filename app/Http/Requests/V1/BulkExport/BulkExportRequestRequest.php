<?php

namespace App\Http\Requests\V1\BulkExport;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BulkExportRequestRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country' => [
                'required',
                Rule::in(['US', 'CA'])
            ],
            'output' => 'required|regex:/^csv$/i',
            'added_after' => 'regex:/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}$/i'
        ];
    }
}

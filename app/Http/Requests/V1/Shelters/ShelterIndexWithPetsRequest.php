<?php

namespace App\Http\Requests\V1\Shelters;

use Illuminate\Foundation\Http\FormRequest;

class ShelterIndexWithPetsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shelter_ids' => 'required|array',
            'start_number' => 'integer',
            'end_number' => 'integer|max(25000)',
            'meta_only' => 'boolean',
        ];
    }
}

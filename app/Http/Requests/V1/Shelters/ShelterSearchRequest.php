<?php

namespace App\Http\Requests\V1\Shelters;

use Illuminate\Foundation\Http\FormRequest;

class ShelterSearchRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_or_zip' => 'required',
            'geo_range' => 'required|integer',
            'adopts_out' => 'boolean',
            'start_number' => 'integer',
            'end_number' => 'integer|min:50,max:250',
        ];
    }
}

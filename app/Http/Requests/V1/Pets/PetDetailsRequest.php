<?php

namespace App\Http\Requests\V1\Pets;

use Illuminate\Foundation\Http\FormRequest;

class PetDetailsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pet_id' => 'required|integer',
        ];
    }
}

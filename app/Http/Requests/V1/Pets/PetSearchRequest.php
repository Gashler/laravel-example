<?php

namespace App\Http\Requests\V1\Pets;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PetSearchRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_or_zip' => 'required',
            'geo_range' => 'required|integer',
            'species' => [
                'required',
                Rule::in(array_keys(config('clans'))),
            ]
        ];
    }
}

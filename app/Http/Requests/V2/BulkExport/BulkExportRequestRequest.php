<?php

namespace App\Http\Requests\V2\BulkExport;

use App\Rules\IntegerOrArrayOfIntegers;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BulkExportRequestRequest extends FormRequest
{
    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    public function all($keys = null)
    {
        $inputs = parent::all();

        // Convert country array into string
        if (isset($inputs['country']) && is_array($inputs['country'])) {
            $inputs['country'] = implode('&', $inputs['country']);
        }

        // Convert single OrgID integer into an array
        if (isset($inputs['orgID']) && !is_array($inputs['orgID'])) {
            $inputs['orgID'] = [$inputs['orgID']];
        }

        return $inputs;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country' => [
                'required',
                Rule::in(['US', 'CA', 'US&CA'])
            ],
            'output' => 'regex:/^json$/i',
            'since_ts' => 'integer',
            'until_ts' => 'integer',
            'options' => 'regex:/^detailsUrl$/i',
            'orgID' => new IntegerOrArrayOfIntegers
        ];
    }
}

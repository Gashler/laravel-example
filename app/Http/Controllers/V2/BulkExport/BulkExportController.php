<?php

namespace App\Http\Controllers\V2\BulkExport;

use App\Http\Responses\V2\BulkExportResponse;
use App\Http\Responses\ResponseInterface;
use App\Http\Controllers\V1\WithResponseInterface;

class BulkExportController implements WithResponseInterface
{
    /**
     * Get instance of Response
     *
     * @return ResponseInterface
     */
    public function responder(): ResponseInterface
    {
        return new BulkExportResponse;
    }
}

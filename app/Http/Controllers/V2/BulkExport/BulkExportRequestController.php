<?php

namespace App\Http\Controllers\V2\BulkExport;

use App\Http\Controllers\V2\BulkExport\BulkExportController;
use App\Http\Requests\V2\BulkExport\BulkExportRequestRequest;
use App\Services\V2\BulkExportService;
use App\Services\V2\BulkExportServiceInterface;
use Illuminate\Http\Request;

class BulkExportRequestController extends BulkExportController
{
    public function service(Request $request): BulkExportServiceInterface
    {
        return new BulkExportService($request->key);
    }

    /**
     * Return an array of pets
     *
     * @param Request $request
     * @return array
     */
    public function pets(BulkExportRequestRequest $request): object
    {
        return $this->responder()->handle(
            $this->service($request)->handlePetsExport($request->all())
        );
    }

    /**
     * Return an array of shelters
     *
     * @param Request $request
     * @return array
     */
    public function shelters(BulkExportRequestRequest $request): object
    {
        return $this->responder()->handle(
            $this->service($request)->handleSheltersExport($request->all())
        );
    }
}

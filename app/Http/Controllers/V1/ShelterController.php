<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\V1\ApiController;
use App\Http\Requests\V1\Shelters\ShelterDetailsRequest;
use App\Http\Requests\V1\Shelters\ShelterDetailsWithPetsRequest;
use App\Http\Requests\V1\Shelters\ShelterIndexWithPetsRequest;
use App\Http\Requests\V1\Shelters\ShelterSearchRequest;
use App\Repositories\ShelterRepository;
use App\Repositories\ApiRepositoryInterface;

class ShelterController extends ApiController
{
    /**
     * Get instance of PetRepository
     *
     * @return ApiRepositoryInterface
     */
    public function repo(): ApiRepositoryInterface
    {
        return new ShelterRepository();
    }

    /**
     * Show the pets at the specified shelter
     *
     * @param ShelterDetailsRequest $request
     * @return ShelterControllerV2@show
     */
    public function details(ShelterDetailsRequest $request)
    {
        return $this->show($request->query->get('shelter_id'));
    }

    /**
     * Show the pets at the specified shelter
     *
     * @param ShelterDetailsWithPetsRequest $request
     * @return ShelterControllerV2@show
     */
    public function detailsWithPets(ShelterDetailsWithPetsRequest $request)
    {
        $id = $request->query->get('shelter_id');
        $with = 'pets';
        $params = $request->query->all();
        return $this->show($id, [$with], $params);
    }

    /**
     * Include pets with the matched shelters
     *
     * @param ShelterDetailsWithPetsRequest $request
     * @return ShelterControllerV2@index
     */
    public function searchWithPets(ShelterIndexWithPetsRequest $request)
    {
        $request->query->add(
            [
                'ids' => $request->query->get('shelter_ids'),
                'with' => 'pets',
                'max_take' => 25000
            ]
        );
        $request->query->remove('shelter_ids');
        return $this->index($request);
    }

    /**
     * Display a listing of the resource.
     *
     * @param array $params
     * @return \App\Responses\V1\ApiResponse
     */
    public function search(ShelterSearchRequest $request)
    {
        $request->query->add(
            [
                'max' => 250,
                'take' => 50,
            ]
        );
        return $this->responder()->handle(
            $this->repo()->search($request->all())
        );
    }
}

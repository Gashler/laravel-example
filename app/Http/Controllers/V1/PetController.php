<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\V1\ApiController;
use App\Http\Requests\V1\Pets\PetDetailsRequest;
use App\Http\Requests\V1\Pets\PetSearchRequest;
use App\Http\Requests\V1\Pets\PetSearchFormRequest;
use App\Repositories\PetRepository;
use App\Repositories\ApiRepositoryInterface;

class PetController extends ApiController
{
    /**
     * Get instance of PetRepository
     *
     * @return ApiRepositoryInterface
     */
    public function repo(): ApiRepositoryInterface
    {
        return new PetRepository;
    }

    /**
     * Show details for the specified pet
     *
     * @param PetDetailsRequest
     * @return PetControllerV2@show
     */
    public function details(PetDetailsRequest $request)
    {
        return $this->show($request->query->get('pet_id'));
    }

    /**
     * Show limited details for the specified pet
     *
     * @param PetDetailsRequest
     * @return PetControllerV2@show
     */
    public function detailsLimited(PetDetailsRequest $request)
    {
        // TODO: configure the limited dataset
        return $this->show($request->query->get('pet_id'));
    }

    /**
     * Search for pets
     *
     * @param PetSearchRequest
     * @return @index
     */
    public function search(PetSearchRequest $request)
    {
        $clanId = config('clans')[$request->query->get('species')]['clan_id'];
        $request->query->add(
            [
                'clan_id' => $clanId,
                'max_take' => 500,
                'take' => 50,
            ]
        );
        return $this->responder()->handle(
            $this->repo()->search($request->query->all())
        );
    }

    /**
     * Get the pet search form
     *
     * @param PetSearchFormRequest $request
     * @return \App\Responses\V1\ApiResponse
     */
    public function searchForm(PetSearchFormRequest $request)
    {
        $clanId = config('clans')[$request->query->get('species')]['clan_id'];
        return $this->responder()->handle(
            $this->repo()->searchForm($clanId)
        );
    }
}

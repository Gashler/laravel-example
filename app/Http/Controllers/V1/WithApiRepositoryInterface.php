<?php

namespace App\Http\Controllers\V1;

use App\Repositories\ApiRepositoryInterface;

interface WithApiRepositoryInterface
{
    public function repo(): ApiRepositoryInterface;
}

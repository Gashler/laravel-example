<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Responses\Response;
use App\Http\Responses\ResponseInterface;
use App\Http\Responses\V1\ApiResponse;
use Illuminate\Http\Request;

abstract class ApiController extends Controller implements
    WithApiRepositoryInterface,
    WithResponseInterface
{
    /**
     * Get instance of ApiResponse
     *
     * @return ApiResponseInterface
     */
    public function responder(): ResponseInterface
    {
        return new ApiResponse;
    }

    /**
     * Display a listing of the resource.
     *
     * @param array $params
     * @return @response
     */
    public function index(Request $request)
    {
        return $this->responder()->handle(
            $this->repo()->index($request->all())
        );
    }

    /**
     * Display the specified resource.
     *
     * @param integer $id
     * @return @response
     */
    public function show(int $id, array $with = [], array $params = [])
    {
        return $this->responder()->handle(
            $this->repo()->show($id, $with, $params)
        );
    }
}

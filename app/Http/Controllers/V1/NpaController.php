<?php

namespace App\Http\Controllers\V1;

use App\Http\Requests\V1\Npa\NpaSubscribeRequest;
use App\Http\Responses\ResponseInterface;
use App\Http\Responses\Response;
use App\Services\NpaServiceInterface;
use App\Services\NpaService;

class NpaController
{
    /**
     * Get instance of PetRepository
     *
     * @return NpaServiceInterface
     */
    public function service(): NpaServiceInterface
    {
        return new NpaService();
    }

    /**
     * Get instance of ApiResponse
     *
     * @return ApiResponseInterface
     */
    public function responder(): ResponseInterface
    {
        return new Response;
    }

    /**
     * Subcribe a user to NPA
     *
     * @return @response
     */
    public function subscribe(NpaSubscribeRequest $request)
    {
        return $this->responder()->handle(
            $this->service()->subscribe(
                $request['email'],
                $request['search_params']
            )
        );
    }
}

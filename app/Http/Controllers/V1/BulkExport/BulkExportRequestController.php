<?php

namespace App\Http\Controllers\V1\BulkExport;

use App\Http\Controllers\V1\BulkExport\BulkExportController;
use App\Http\Requests\V1\BulkExport\BulkExportRequestRequest;
use App\Services\V1\BulkExportService;
use App\Services\V1\BulkExportServiceInterface;
use Illuminate\Http\Request;

class BulkExportRequestController extends BulkExportController
{
    public function service(Request $request): BulkExportServiceInterface
    {
        return new BulkExportService($request->key);
    }

    /**
     * Return an array of pets
     *
     * @param Request $request
     * @return array
     */
    public function pets(BulkExportRequestRequest $request): object
    {
        return $this->responder()->handle(
            $this->service($request)->pets([$request->country], $request->limit)
        );
    }

    /**
     * Return an array of shelters
     *
     * @param Request $request
     * @return array
     */
    public function shelters(BulkExportRequestRequest $request): object
    {
        return $this->responder()->handle(
            $this->service($request)->shelters(
                [$request->country],
                $request->added_after,
                $request->limit
            )
        );
    }
}

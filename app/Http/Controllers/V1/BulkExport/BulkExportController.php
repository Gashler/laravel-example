<?php

namespace App\Http\Controllers\V1\BulkExport;

use App\Http\Responses\Response;
use App\Http\Responses\ResponseInterface;
use App\Http\Responses\OutputCsv;
use App\Http\Controllers\V1\WithResponseInterface;

class BulkExportController implements WithResponseInterface
{
    /**
     * Get instance of Response
     *
     * @return ResponseInterface
     */
    public function responder(): ResponseInterface
    {
        // Use the default response (raw) in CSV format
        return new Response(new OutputCsv);
    }
}

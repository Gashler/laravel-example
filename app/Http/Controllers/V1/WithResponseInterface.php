<?php

namespace App\Http\Controllers\V1;

use App\Http\Responses\ResponseInterface;

interface WithResponseInterface
{
    public function responder(): ResponseInterface;
}

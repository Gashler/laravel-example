<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Clans
    |--------------------------------------------------------------------------
    |
    | A list of animal species (clans) and their properties
    |
    */

    'dog' => [
        'clan_id' => 1,
        'clan_name' => 'dog',
        'clan_name_plural' => 'dogs',
        'active_p' => true,
        'tech_name' => 'dog',
        'family_label' => 'breed',
        'clan_name_pretty' => 'Dog',
        'clan_name_plural_pretty' => 'Dogs',
    ],
    'cat' => [
        'clan_id' => 2,
        'clan_name' => 'cat',
        'clan_name_plural' => 'cats',
        'active_p' => true,
        'tech_name' => 'cat',
        'family_label' => 'breed',
        'clan_name_pretty' => 'Cat',
        'clan_name_plural_pretty' => 'Cats',
    ],
    'rabbit' => [
        'clan_id' => 3,
        'clan_name' => 'rabbit',
        'clan_name_plural' => 'rabbits',
        'active_p' => true,
        'tech_name' => 'rabbit',
        'family_label' => 'breed',
        'clan_name_pretty' => 'Rabbit',
        'clan_name_plural_pretty' => 'Rabbits',
    ],
    'bird' => [
        'clan_id' => 5,
        'clan_name' => 'bird',
        'clan_name_plural' => 'birds',
        'active_p' => true,
        'tech_name' => 'bird',
        'family_label' => 'breed',
        'clan_name_pretty' => 'Bird',
        'clan_name_plural_pretty' => 'Birds',
    ],
    'horse' => [
        'clan_id' => 6,
        'clan_name' => 'horse',
        'clan_name_plural' => 'horses',
        'active_p' => true,
        'tech_name' => 'horse',
        'family_label' => 'breed',
        'clan_name_pretty' => 'Horse',
        'clan_name_plural_pretty' => 'Horses',
    ],
    'small_animal' => [
        'clan_id' => 4,
        'clan_name' => 'small animal',
        'clan_name_plural' => 'small animals',
        'active_p' => true,
        'tech_name' => 'small_animal',
        'family_label' => 'species',
        'clan_name_pretty' => 'Small Animal',
        'clan_name_plural_pretty' => 'Small Animals',
    ],
    'reptile' => [
        'clan_id' => 7,
        'clan_name' => 'reptile, amphibian, and/or fish',
        'clan_name_plural' => 'reptiles, amphibians, and/or fish',
        'active_p' => true,
        'tech_name' => 'reptile',
        'family_label' => 'species',
        'clan_name_pretty' => 'Reptile, Amphibian, and/or Fish',
        'clan_name_plural_pretty' => 'Reptiles, Amphibians, and/or Fish',
    ],
    'farm_animal' => [
        'clan_id' => 8,
        'clan_name' => 'farm-type animal',
        'clan_name_plural' => 'farm-type animals',
        'active_p' => true,
        'tech_name' => 'farm_animal',
        'family_label' => 'species',
        'clan_name_pretty' => 'Farm-Type Animal',
        'clan_name_plural_pretty' => 'Farm-Type Animals',
    ],
];

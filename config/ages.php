<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Ages
    |--------------------------------------------------------------------------
    |
    | A list of animal ages by clan
    |
    */

    1 => [
        'puppy',
        'young',
        'adult',
        'senior',
    ],
    2 => [
        'kitten',
        'young',
        'adult',
        'senior',
    ],
    3 => [
        'baby',
        'young',
        'adult',
        'senior',
    ],
    4 => [
        'baby',
        'young',
        'adult',
        'senior',
    ],
    5 => [
        'baby',
        'young',
        'adult',
        'senior',
    ],
    6 => [
        'baby',
        'young',
        'adult',
        'senior',
    ],
    7 => [
        'baby',
        'young',
        'adult',
        'senior',
    ],
    8 => [
        'baby',
        'young',
        'adult',
        'senior',
    ],
];

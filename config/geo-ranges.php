<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Geo Ranges
    |--------------------------------------------------------------------------
    |
    | A list of geo ranges for search forms
    |
    */

    [
        'value' => '35',
        'label' => 'Less than 35 miles (55 km)'
    ],
    [
        'value' => '50',
        'label' => 'Less than 50 miles (80 km)'
    ],
    [
        'value' => '75',
        'label' => 'Less than 75 miles (120 km)'
    ],
    [
        'value' => '100',
        'label' => 'Less than 100 miles (160 km)'
    ],
    [
        'value' => '250',
        'label' => 'Less than 250 miles (400 km)'
    ]
];
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Company Defaults
    |--------------------------------------------------------------------------
    |
    | Default values for company information
    |
    */

    'base_url' => env('COMPANY_BASE_URL', 'https://adoptapet.com'),
];

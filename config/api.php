<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API Defaults
    |--------------------------------------------------------------------------
    |
    | Default values that apply across API endpoints
    |
    */

    'max_take' => env('API_MAX_TAKE', 250),
    'take' => env('API_TAKE', 50),
    'cache_lifespan' => env('API_CACHE_LIFESPAN', 86400),
    'download_url' => 'downloads', // will replace with URL to S3 bucket
    'pet_uploads_url' => 'https://pet-uploads.adoptapet.com',
    'bulk_export_file_expiration_time' => 60 * 60 // 1 hour
];

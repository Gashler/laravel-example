<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Legacy API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api-legacy" middleware group. Enjoy building your API!
|
*/

Route::prefix('search')->group(
    function () {
        /**
         * Pets
         */
        Route::get('search_form', 'V1\PetController@searchForm');
        Route::get('pet_search', 'V1\PetController@search');
        Route::get('pet_details', 'V1\PetController@details');
        Route::get('limited_pet_details', 'V1\PetController@detailsLimited');

        /**
         * Shelters
         */
        Route::get('shelter_search', 'V1\ShelterController@search');
        Route::get('shelter_details', 'V1\ShelterController@details');
        Route::get('pets_at_shelter', 'V1\ShelterController@detailsWithPets');
        Route::get('pets_at_shelters', 'V1\ShelterController@searchWithPets');
    }
);

/**
 * NPA
 */
Route::post('npa/subscribe', 'V1\NpaController@subscribe');

/**
 * Bulk exports
 */
Route::prefix('bulk-exports')->group(
    function () {
        Route::get('pets', 'V1\BulkExport\BulkExportRequestController@pets');
        Route::get('shelters', 'V1\BulkExport\BulkExportRequestController@shelters');
    }
);

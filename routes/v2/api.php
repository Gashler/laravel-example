<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Legacy API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api-legacy" middleware group. Enjoy building your API!
|
*/

/**
 * Bulk exports
 */
Route::prefix('bulk-exports')->group(function () {
    Route::prefix('/v2/')->group(function () {
        /**
         * Pets
         */
        Route::get('all_pets', 'V2\BulkExportController@petsAll');
        Route::get('pets_added', 'V2\BulkExportController@petsAdded');
        Route::get('pets_updated', 'V2\BulkExportController@petsUpdated');
        Route::get('pets_deleted', 'V2\BulkExportController@petsDeleted');

        /**
         * Shelters
         */
        Route::get('all_shelters', 'V2\BulkExportController@sheltersAll');
        Route::get('shelters_added', 'V2\BulkExportController@sheltersAdded');
        Route::get('shelters_updated', 'V2\BulkExportController@sheltersUpdated');
        Route::get('shelters_deleted', 'V2\BulkExportController@sheltersDeleted');
    });
});

<?php

namespace Tests\Unit;

use Symfony\Component\HttpFoundation\StreamedResponse;
use Tests\TestCase;

use App\Http\Responses\OutputCsv;

class OutputCsvTest extends TestCase
{
    protected $handler;

    public function setup(): void
    {
        parent::setup();
        $this->outputCsv = new OutputCsv;
    }

    /**
     * Assertions on the basic instantiation of the OutputCsv class
     *
     * @return void
     */
    public function testClassInstance(): void
    {
        $this->assertInstanceOf(
            OutputCsv::class,
            $this->outputCsv,
            'Expect $this->outputCsv to return instance of OutputCsv'
        );
    }

    /**
     * Assertions on the output of the output method for the OutputCsv class
     *
     * @return void
     */
    public function testOutput()
    {
        $results = \App\Models\V1\Pet::limit(1)->get();
        $response = $this->outputCsv->output($results);
        $this->outputCsv->callback($results); // for ensuring code coverage
        $this->assertInstanceOf(StreamedResponse::class, $response, 'response streams a CSV file');
    }
}

<?php

namespace Tests\Unit;

use Tests\TestCase;

use App\Exceptions\Handler;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use \Symfony\Component\HttpFoundation\Response;
use \Exception;

class ExceptionHandlerUnitTest extends TestCase
{
    protected $handler;

    public function setup() : void
    {
        parent::setup();
        // Create an instance of Handler with a base Container
        $this->handler = new Handler(new Container);
    }
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testBaseHandler()
    {
        // Mock up an Exception to verify is thrown later
        $mockException = $this->createMock(Exception::class);

        $this->assertInstanceOf(
            Response::class,
            $this->handler->render(
                $this->createMock(Request::class),
                $mockException
            ),
            'Exception handler render() method returns a valid Response Object'
        );

        $this->expectException(class_basename($mockException));
        $this->handler->report(
            $this->createMock(Exception::class)
        );
    }

    public function testHandlerReportAllowReturnVoid()
    {
        try {
            $returnValue = $this->handler->report(
                $this->createMock(Exception::class)
            );
        } catch (Exception $e) {
            $this->assertInstanceOf(Exception::class, $e);
        }

        $this->assertTrue(!isset($returnValue));
    }
}

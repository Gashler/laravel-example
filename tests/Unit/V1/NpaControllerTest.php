<?php

namespace Tests\Unit\V1;

use \Faker\Factory as FakerFactory;
use App\Exceptions\NpaSubscribeException;
use App\Http\Requests\V1\Npa\NpaSubscribeRequest;
use App\Models\V1\PetSearchQuery;
use App\Http\Controllers\V1\NpaController;
use Tests\TestCase;

class NpaControllerTest extends TestCase
{
    public $controller;
    protected $query;

    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->controller = new NpaController;
        $faker = FakerFactory::create();
        $this->email = $faker->unique()->safeEmail;
        $this->query = factory(PetSearchQuery::class)->make();
    }

    /**
     * Assertions on the basic instantiation of the NpaController
     *
     * @return void
     */
    public function testPetControllerInstance(): void
    {
        $this->assertInstanceOf(NpaController::class, $this->controller, 'Expect controller to be instance of App\Controllers\NpaController');
    }

    /**
     * Assertions on the object returned by show method in
     * NpaController class for Success case
     *
     * @return void
     */
    public function testSubscribeSuccess(): void
    {
        $request = NpaSubscribeRequest::create(
            '/npa/subscribe',
            'POST',
            [
                'email' => $this->email,
                'search_params' => http_build_query($this->query->toArray())
            ]
        );
        $response = $this->controller->subscribe($request);
        $this->assertEquals(200, $response->getStatusCode(), 'search() returns a 200 response');
        $this->assertInstanceOf(\Illuminate\Http\JsonResponse::class, $response, 'response body contains JSON');
    }

    /**
     * Assertions on the object returned by show method in
     * NpaController class for duplicate email case
     *
     * @return void
     */
    public function testSubscribeDuplicateEmail(): void
    {
        $request = NpaSubscribeRequest::create(
            '/npa/subscribe',
            'POST',
            [
                'email' => 'steve@adoptapet.com',
                'search_params' => http_build_query($this->query->toArray())
            ]
        );
        $this->expectException(NpaSubscribeException::class);
        $response = $this->controller->subscribe($request);
        $this->assertEquals(450, $response->getStatusCode(), 'subscribe() returns a 450 response');
    }
}

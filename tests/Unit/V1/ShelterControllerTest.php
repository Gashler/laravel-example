<?php

namespace Tests\Unit\V1;

use App\Http\Controllers\V1\ShelterController;
use App\Repositories\ShelterRepository;
use App\Http\Responses\Response;
use App\Http\Requests\V1\Shelters\ShelterDetailsRequest;
use App\Http\Requests\V1\Shelters\ShelterDetailsWithPetsRequest;
use App\Http\Requests\V1\Shelters\ShelterSearchRequest;
use App\Http\Requests\V1\Shelters\ShelterIndexWithPetsRequest;
use App\Models\V1\Pet;
use App\Models\V1\Shelter;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class ShelterControllerTest extends TestCase
{
    public $controller;
    protected $query;

    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->controller = new ShelterController;
        $this->query = [
            'city_or_zip' => 84606,
            'geo_range' => 50,
            'shelter_id' => 98957,
            'shelter_ids' => [98957, 94752],
            'invalid_string' => '00000',
        ];
    }

    /**
     * Assertions on the basic instantiation of the V1 ShelterController
     *
     * @return void
     */
    public function testControllerInstance(): void
    {
        $this->assertInstanceOf(ShelterController::class, $this->controller, 'Expect controller to be instance of V1\ShelterController');
        $this->assertInstanceOf(ShelterRepository::class, $this->controller->repo(), 'Expect repo() to return instance of ShelterRepository');
        $this->assertInstanceOf(Response::class, $this->controller->responder(), 'Expect responder() to return instance of V1\Response');
    }

    /**
     * Assertions on the response object returned by details method in
     * V1\ShelterController class for Success case
     *
     * @return void
     */
    public function testDetailsSuccess(): void
    {
        $request = ShelterDetailsRequest::create(
            '/shelter_details',
            'GET',
            [
                'shelter_id' => $this->query['shelter_id']
            ]
        );
        $response = $this->controller->details($request);
        $this->assertEquals(200, $response->getStatusCode(), 'details() returns a 200 response');
        $this->assertInstanceOf(\Illuminate\Http\JsonResponse::class, $response, 'response body contains JSON');
        $this->assertInstanceOf(Shelter::class, $response->getOriginalContent()['body'], 'response body contains a Shelter model instance');
        $this->assertEquals($this->query['shelter_id'], $response->getData()->body->shelter_id, 'Shelter model instance shelter_id matches the argument provided to details method in request');
    }

    /**
     * Assertions on the response object returned by details method in
     * V1\ShelterController class for Not Found case
     *
     * @return void
     */
    public function testDetailsNotFound(): void
    {
        $request = ShelterDetailsRequest::create(
            '/search/shelter_details',
            'GET',
            [
                'shelter_id' => $this->query['invalid_string'],
            ]
        );
        $this->expectException(ModelNotFoundException::class);
        $this->controller->details($request);
    }

    /**
     * Assertions on the response object returned by search method in
     * V1\ShelterController class for Success case
     *
     * @return void
     */
    public function testSearch(): void
    {
        $request = ShelterSearchRequest::create(
            '/search/shelter_search',
            'GET',
            [
                'city_or_zip' => $this->query['city_or_zip'],
                'geo_range' => $this->query['geo_range'],
            ]
        );
        $response = $this->controller->search($request);
        $this->assertEquals(200, $response->getStatusCode(), 'search() returns a 200 response');
        $this->assertInstanceOf(\Illuminate\Http\JsonResponse::class, $response, 'response body contains JSON');
        $this->assertInstanceOf(Shelter::class, $response->getOriginalContent()['body'][0], 'response body contains Shelter model instances');
    }

    /**
     * Assertions on the response object returned by detailsWithPets method in
     * V1\ShelterController class for Success case
     *
     * @return void
     */
    public function testDetailsWithPets(): void
    {
        $request = ShelterDetailsWithPetsRequest::create(
            '/search/pets_at_shelter',
            'GET',
            [
                'shelter_id' => $this->query['shelter_id']
            ]
        );
        $response = $this->controller->detailsWithPets($request);
        $this->assertEquals(200, $response->getStatusCode(), 'search() returns a 200 response');
        $this->assertInstanceOf(\Illuminate\Http\JsonResponse::class, $response, 'response body contains JSON');
        $this->assertInstanceOf(Shelter::class, $response->getOriginalContent()['body'], 'response body contains Shelter model instance');
        $this->assertInstanceOf(Pet::class, $response->getOriginalContent()['body']->pets[0], 'response body contains Pet model instances');
    }

    /**
     * Assertions on the response object returned by detailsWithPets method in
     * V1\ShelterController class for Not Found case
     *
     * @return void
     */
    public function testDetailsWithPetsNotFound(): void
    {
        $request = ShelterDetailsWithPetsRequest::create(
            '/search/pets_at_shelter',
            'GET',
            [
                'shelter_id' => $this->query['invalid_string'],
            ]
        );
        $this->expectException(ModelNotFoundException::class);
        $this->controller->detailsWithPets($request);
    }

    /**
     * Assertions on the response object returned by searchWithPets method in
     * V1\ShelterController class for Success case
     *
     * @return void
     */
    public function testSearchWithPets(): void
    {
        $request = ShelterIndexWithPetsRequest::create(
            '/search/pets_at_shelters',
            'GET',
            [
                'shelter_ids' => $this->query['shelter_ids']
            ]
        );
        $response = $this->controller->searchWithPets($request);
        $this->assertEquals(200, $response->getStatusCode(), 'search() returns a 200 response');
        $this->assertInstanceOf(\Illuminate\Http\JsonResponse::class, $response, 'response body contains JSON');
        $this->assertInstanceOf(Shelter::class, $response->getOriginalContent()['body'][0], 'response body contains Shelter model instances');
        $this->assertInstanceOf(Pet::class, $response->getOriginalContent()['body'][0]->pets[0], 'response body contains Pet model instances');
    }
}

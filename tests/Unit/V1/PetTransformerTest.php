<?php

namespace Tests\Unit\V1;

use Tests\TestCase;
use App\Transformers\PetTransformer;
use App\Models\V1\Pet;

class PetTransformerTest extends TestCase
{
    public $transformer;

    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp() : void
    {
        parent::setUp();
        $this->transform = new PetTransformer;
    }

    /**
     * Assertions on the basic instantiation of the PetTransformer
     *
     * @return void
     */
    public function testTransformerInstance() : void
    {
        $this->assertInstanceOf(PetTransformer::class, $this->transform, 'Expect transformer to be instance of App\Transformers\PetTransformer');
    }

    /**
     * Assertions on the data returned by "to" method in
     * PetTransformer class
     *
     * @return void
     */
    public function testTransformTo() : void
    {
        $result = $this->transform->to(
            'form.ages',
            [
                [
                    'age' => 'puppy'
                ]
            ]
        );
        $this->assertEquals('Puppy', $result[0]['label'], 'Data is transformed for form usage');
        $this->assertEquals('puppy', $result[0]['value'], 'Data is transformed for form usage');
    }
}

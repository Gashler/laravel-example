<?php

namespace Tests\Unit\V1;

use App\Http\Controllers\V1\PetController;
use App\Repositories\PetRepository;
use App\Http\Responses\Response;
use App\Http\Requests\V1\Pets\PetDetailsRequest;
use App\Http\Requests\V1\Pets\PetSearchFormRequest;
use App\Http\Requests\V1\Pets\PetSearchRequest;
use App\Models\V1\Pet;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class PetControllerTest extends TestCase
{
    public $controller;
    protected $query;

    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->controller = new PetController;
        $petQueries = [
            [
                'species' => 'cat',
                'age_young' => 'young',
                'breed' => 'Himalayan',
                'color' => 'Black',
                'color_id' => 1,
                'hair' => 'medium',
                'sex' => 'm',
            ],
            [
                'species' => 'dog',
                'age_young' => 'puppy',
                'breed' => 'Italian Greyhound',
                'color' => 'Gray/Blue/Silver/Salt & Pepper',
                'color_id' => 1,
                'hair' => 'short',
                'sex' => 'm',
            ]
        ];
        $this->query = [
            'city_or_zip' => 84606,
            'geo_range' => 50,
            'pet' => $petQueries[rand(0, 1)],
            'pet_id' => 28108016,
            'invalid_string' => '00000'
        ];
    }

    /**
     * Assertions on the basic instantiation of the V1 PetController
     *
     * @return void
     */
    public function testControllerInstance(): void
    {
        $this->assertInstanceOf(PetController::class, $this->controller, 'Expect controller to be instance of V1\PetController');
        $this->assertInstanceOf(PetRepository::class, $this->controller->repo(), 'Expect repo() to return instance of PetRepository');
        $this->assertInstanceOf(Response::class, $this->controller->responder(), 'Expect responder() to return instance of V1\Response');
    }

    /**
     * Assertions on the response object returned by details method in
     * V1\PetController class for Success case
     *
     * @return void
     */
    public function testDetailsSuccess(): void
    {
        $request = PetDetailsRequest::create(
            '/pet_details',
            'GET',
            [
                'pet_id' => $this->query['pet_id']
            ]
        );
        $response = $this->controller->details($request);
        $this->assertEquals(200, $response->getStatusCode(), 'details() returns a 200 response');
        $this->assertInstanceOf(\Illuminate\Http\JsonResponse::class, $response, 'response body contains JSON');
        $this->assertInstanceOf(Pet::class, $response->getOriginalContent()['body'], 'response body contains a Pet model instance');
        $this->assertEquals($this->query['pet_id'], $response->getData()->body->pet_id, 'Pet model instance pet_id matches the argument provided to details method in request');
    }

    /**
     * Assertions on the response object returned by details method in
     * V1\PetController class for Not Found case
     *
     * @return void
     */
    public function testDetailsNotFound(): void
    {
        $request = PetDetailsRequest::create(
            '/search/pet_details',
            'GET',
            [
                'pet_id' => $this->query['invalid_string'],
            ]
        );
        $this->expectException(ModelNotFoundException::class);
        $this->controller->details($request);
    }

    /**
     * Assertions on the response object returned by detailsLimited method in
     * V1\PetController class for Success case
     *
     * @return void
     */
    public function testDetailsLimitedSuccess(): void
    {
        $request = PetDetailsRequest::create(
            '/limited_pet_details',
            'GET',
            [
                'pet_id' => $this->query['pet_id']
            ]
        );
        $response = $this->controller->detailsLimited($request);
        $this->assertEquals(200, $response->getStatusCode(), 'details() returns a 200 response');
        $this->assertInstanceOf(\Illuminate\Http\JsonResponse::class, $response, 'response body contains JSON');
        $this->assertInstanceOf(Pet::class, $response->getOriginalContent()['body'], 'response body contains a Pet model instance');
        $this->assertEquals($this->query['pet_id'], $response->getData()->body->pet_id, 'Pet model instance pet_id matches the argument provided to details method in request');
    }

    /**
     * Assertions on the response object returned by detailsLimited method in
     * V1\PetController class for Not Found case
     *
     * @return void
     */
    public function testDetailsLimitedNotFound(): void
    {
        $request = PetDetailsRequest::create(
            '/search/limited_pet_details',
            'GET',
            [
                'pet_id' => $this->query['invalid_string'],
            ]
        );
        $this->expectException(ModelNotFoundException::class);
        $this->controller->detailsLimited($request);
    }

    /**
     * Assertions on the response object returned by searchForm method in
     * V1\PetController class for Success case
     *
     * @return void
     */
    public function testSearchFormSuccess(): void
    {
        $request = PetSearchFormRequest::create(
            '/search/search_form',
            'GET',
            [
                'species' => $this->query['pet']['species']
            ]
        );
        $response = $this->controller->searchForm($request);
        $this->assertEquals(200, $response->getStatusCode(), 'searchForm() returns a 200 response');
        $this->assertInstanceOf(\Illuminate\Http\JsonResponse::class, $response, 'response body contains JSON');
        $this->assertEquals($this->query['pet']['age_young'], $response->getData()->body->age[1]->value, 'response contains appropriate ages for the species');
        $this->assertEquals($this->query['pet']['breed'], $response->getData()->body->breed_id[1]->label, 'response contains appropriate breeds for the species');
        $this->assertEquals($this->query['pet']['color'], $response->getData()->body->color_id[1]->label, 'response contains appropriate colors for the species');
        $this->assertEquals($this->query['geo_range'], $response->getData()->body->geo_range[1]->value, 'response contains geo ranges');
        $this->assertEquals($this->query['pet']['hair'], $response->getData()->body->hair[1]->value, 'response contains appropriate hair lengths for species');
        $this->assertEquals($this->query['pet']['sex'], $response->getData()->body->sex[1]->value, 'response contains appropriate sexes for species');
    }

    /**
     * Assertions on the response object returned by search method in
     * V2\PetController class for Success case
     *
     * @return void
     */
    public function testSearch(): void
    {
        /**
         * Only dogs can have pet_size_range_id filters
         * so we force the query to be a dog
         */

        $request = PetSearchRequest::create(
            '/pet_search',
            'GET',
            [
                'age' => 'adult',
                'color_id' => 45, // Hardcode a dog color ID, since we force a dog clan id in the query
                'city_or_zip' => $this->query['city_or_zip'],
                'geo_range' => $this->query['geo_range'],
                'pet_size_range_id' => 1,
                'species' => 'dog', // Only dogs can be filtered on pet_size_range_id, so hardcode the dog clan id here
                'start_number' => 1,
                'end_number' => 1000 // request more than allowed to test max functionality
            ]
        );
        $response = $this->controller->search($request);
        $this->assertEquals(200, $response->getStatusCode(), 'search() returns a 200 response');
        $this->assertInstanceOf(\Illuminate\Http\JsonResponse::class, $response, 'response body contains JSON');
        $this->assertInstanceOf(\Illuminate\Database\Eloquent\Collection::class, $response->getOriginalContent()['body'], 'response body contains collection');
        $this->assertInstanceOf(Pet::class, $response->getOriginalContent()['body'][0], 'response body contains Pet model instances');
    }
}

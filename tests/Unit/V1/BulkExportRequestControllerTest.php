<?php

namespace Tests\Unit\V1;

use App\Http\Controllers\V1\BulkExport\BulkExportRequestController;
use App\Http\Requests\V1\BulkExport\BulkExportRequestRequest;
use Tests\TestCaseWithApiKey;

class BulkExportRequestControllerTest extends TestCaseWithApiKey
{
    public $controller;

    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->controller = new BulkExportRequestController;
    }

    /**
     * Assertions on the basic instantiation of the V1 BulkExportRequestController
     *
     * @return void
     */
    public function testControllerInstance(): void
    {
        $this->assertInstanceOf(
            BulkExportRequestController::class,
            $this->controller,
            'Expect controller to be instance of V1\BulkExportRequestController'
        );
    }

    /**
     * Assertions on the response object returned by pets method in
     * V1\BulkExportRequestController class for Success case
     *
     * @return void
     */
    public function testPetsSuccess(): void
    {
        $request = BulkExportRequestRequest::create(
            '/bulk-export/pets',
            'GET',
            [
                'key' => $this->apiKey,
                'country' => ['US'],
                'output' => 'csv',
                'limit' => 1
            ]
        );
        $response = $this->controller->pets($request);
        $this->assertEquals(200, $response->getStatusCode(), 'pets returns a 200 response');
    }

    /**
     * Assertions on the response object returned by shelters method in
     * V1\BulkExportRequestController class for Success case
     *
     * @return void
     */
    public function testSheltersSuccess(): void
    {
        $request = BulkExportRequestRequest::create(
            '/bulk-export/shelters',
            'GET',
            [
                'key' => $this->apiKey,
                'country' => ['US'],
                'output' => 'csv',
                'limit' => 1
            ]
        );
        $response = $this->controller->shelters($request);
        $this->assertEquals(200, $response->getStatusCode(), 'shelters returns a 200 response');
    }
}

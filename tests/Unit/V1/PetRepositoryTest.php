<?php

namespace Tests\Unit\V1;

use Tests\TestCase;
use App\Repositories\PetRepository;
use App\Models\V1\Pet;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PetRepositoryTest extends TestCase
{
    public $repo;
    protected $params;

    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->repo = new PetRepository;
        $petParams = [
            [
                'species' => 'cat',
                'age_young' => 'young',
                'breed' => 'Himalayan',
                'color' => 'Black',
                'hair' => 'medium',
                'sex' => 'm',
            ],
            [
                'species' => 'dog',
                'age_young' => 'puppy',
                'breed' => 'Italian Greyhound',
                'color' => 'Gray/Blue/Silver/Salt & Pepper',
                'hair' => 'short',
                'sex' => 'm',
            ]
        ];
        $this->params = [
            'bonded_pair' => false,
            'breed_id' => 972,
            'clan_id' => 2,
            'city_or_zip' => 90210,
            'color_id' => 46,
            'hair' => 'short',
            'geo_range' => 50,
            'pet_id' => 28108016,
            'invalid_string' => '00000',
            'species' => rand(0, 1) ? 'cat' : 'dog',
            'sex' => rand(0, 1) ? 'm' : 'f',
            'special_needs' => false,
            'pet' => $petParams[0]
        ];
    }

    /**
     * Assertions on the basic instantiation of the PetRepository
     *
     * @return void
     */
    public function testRepositoryInstance(): void
    {
        $this->assertInstanceOf(PetRepository::class, $this->repo, 'Expect repository to be instance of App\Repositories\PetRepository');
    }

    /**
     * Assertions on the object returned by show method in
     * PetRepository class for Success case
     *
     * @return void
     */
    public function testShowSuccess(): void
    {
        $result = $this->repo->show($this->params['pet_id']);
        $this->assertInstanceOf(Pet::class, $result, 'result is a Pet model instance');
        $this->assertEquals($this->params['pet_id'], $result->pet_id, 'Pet model instance pet_id matches the provided id argument');
    }

    /**
     * Assertions on the object returned by show method in
     * PetRepository class for Not Found case
     *
     * @return void
     */
    public function testShowNotFound(): void
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repo->show($this->params['invalid_string']);
    }

    /**
     * Assertions on the objects returned by search method in
     * PetRepository class for Success case
     *
     * @return void
     */
    public function testSearchSuccess(): void
    {
        $result = $this->repo->search(
            [
                'bonded_pair' => $this->params['bonded_pair'],
                'breed_id' => $this->params['breed_id'],
                'city_or_zip' => $this->params['city_or_zip'],
                'clan_id' => $this->params['clan_id'],
                'geo_range' => $this->params['geo_range'],
                'hair' => $this->params['hair'],
                'sex' => $this->params['sex'],
                'special_needs' => $this->params['special_needs'],
            ]
        );
        $this->assertInstanceOf(Pet::class, $result[0], 'response body contains Pet model instances');
    }

    /**
     * Assertions on the objects returned by searchForm method in
     * ApiRepository class for Success case
     *
     * @return void
     */
    public function testSearchFormSuccess(): void
    {
        $result = $this->repo->searchForm($this->params['clan_id']);
        $this->assertEquals($this->params['pet']['age_young'], $result['age'][1]['value'], 'result contains appropriate ages for the species');
        $this->assertEquals($this->params['pet']['breed'], $result['breed_id'][1]['label'], 'result contains appropriate breeds for the species');
        $this->assertEquals($this->params['pet']['color'], $result['color_id'][1]['label'], 'result contains appropriate colors for the species');
        $this->assertEquals($this->params['geo_range'], $result['geo_range'][1]['value'], 'result contains geo ranges');
        $this->assertEquals($this->params['pet']['hair'], $result['hair'][1]['value'], 'result contains appropriate hair lengths for species');
        $this->assertEquals($this->params['pet']['sex'], $result['sex'][1]['value'], 'result contains appropriate sexes for species');
    }

    /**
     * Assertions on the data returned by the getBreedIds method in the
     * ApiRepository class
     *
     * @return void
     */
    public function testGetBreedIds(): void
    {
        $result = $this->repo->getBreedIds(1); // providing the clan ID for dogs
        $this->assertContains(187, $result, 'result contains appropriate breed IDs for the species');
    }

    /**
     * Assertions on the data returned by the getColorIds method in the
     * ApiRepository class
     *
     * @return void
     */
    public function testGetColorIds(): void
    {
        $result = $this->repo->getColorIds(1); // providing the clan ID for dogs
        $this->assertContains(152, $result, 'result contains appropriate color IDs for the species');
    }

    /**
     * Assertions on the data returned by the getSizeRangeIds method in the
     * ApiRepository class
     *
     * @return void
     */
    public function testGetSizeRangeIds(): void
    {
        $result = $this->repo->getSizeRangeIds(1); // providing the clan ID for dogs
        $this->assertContains(4, $result, 'result contains appropriates size range IDs for the species');
    }
}

<?php

namespace Tests\Unit\V1;

use DB;
use App\Services\V1\BulkExportService;
use Tests\TestCaseWithApiKey;

class BulkExportServiceTest extends TestCaseWithApiKey
{
    public $service;

    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->service = new BulkExportService($this->apiKey);
        $this->petHarborApiKey = DB::table('public.partners')->where('tech_name', 'pet_harbor')->pluck('api_key')[0];
    }

    /**
     * Assertions on the basic instantiation of the BulkExportService
     *
     * @return void
     */
    public function testPetServiceInstance(): void
    {
        $this->assertInstanceOf(BulkExportService::class, $this->service, 'Expect service to be instance of App\Services\BulkExportService');
    }

    /**
     * Assertions on the objects returned by pets method in
     * BulkExportService class
     *
     * @return void
     */
    public function testPets($apiKey = null): void
    {
        $apiKey = $apiKey ?? $this->apiKey;
        $service = new BulkExportService($apiKey);
        $pet = $service->pets(['US', 'CA'], 10)[0]->getAttributes();
        $this->assertArrayHasKey('pet_id', $pet, 'Expect results to contain pet_id attributes');
        $this->assertArrayHasKey('shelter_id', $pet, 'Expect results to contain shelter_id attributes');
        $this->assertArrayHasKey('pet_name', $pet, 'Expect results to contain pet_name attributes');
        $this->assertArrayHasKey('shelter_reference_code', $pet, 'Expect results to contain shelter_reference_code attributes');
        $this->assertArrayHasKey('species', $pet, 'Expect results to contain species attributes');
        $this->assertArrayHasKey('primary_breed', $pet, 'Expect results to contain primary_breed attributes');
        $this->assertArrayHasKey('secondary_breed', $pet, 'Expect results to contain secondary_breed attributes');
        $this->assertArrayHasKey('color', $pet, 'Expect results to contain color attributes');
        $this->assertArrayHasKey('sex', $pet, 'Expect results to contain sex attributes');
        $this->assertArrayHasKey('age', $pet, 'Expect results to contain age attributes');
        $this->assertArrayHasKey('size', $pet, 'Expect results to contain size attributes');
        $this->assertArrayHasKey('hair_length', $pet, 'Expect results to contain hair_length attributes');
        $this->assertArrayHasKey('photo_url', $pet, 'Expect results to contain photo_Url attributes');
        $this->assertArrayHasKey('description', $pet, 'Expect results to contain description attributes');
        $this->assertArrayHasKey('special_needs_p', $pet, 'Expect results to contain special_needs_p attributes');
        $this->assertArrayHasKey('purebred_p', $pet, 'Expect results to contain purebred_p attributes');
        $this->assertArrayHasKey('shots_current_p', $pet, 'Expect results to contain shots_current_p attributes');
        $this->assertArrayHasKey('housetrained_p', $pet, 'Expect results to contain housetrained_p attributes');
        $this->assertArrayHasKey('declawed_p', $pet, 'Expect results to contain declawed_p attributes');
        $this->assertArrayHasKey('good_with_kids_p', $pet, 'Expect results to contain good_with_kids_p attributes');
        $this->assertArrayHasKey('good_with_dogs_p', $pet, 'Expect results to contain good_with_dogs_p attributes');
        $this->assertArrayHasKey('good_with_cats_p', $pet, 'Expect results to contain good_with_cats_p attributes');
        $this->assertArrayHasKey('spayed_neutered_p', $pet, 'Expect results to contain spayed_neutered_p attributes');
        $this->assertArrayHasKey('pet_uploaded_timestamp', $pet, 'Expect results to contain pet_uploaded_timestamp attributes');
        $this->assertArrayHasKey('photo_uploaded_timestamp', $pet, 'Expect results to contain photo_uploaded_timestamp attributes');
        $this->assertArrayHasKey('shelter_name', $pet, 'Expect results to contain shelter_name attributes');
        $this->assertArrayHasKey('addr_country_code', $pet, 'Expect results to contain addr_country_code attributes');
        $this->assertArrayHasKey('addr_state_code', $pet, 'Expect results to contain addr_state_code attributes');
        $this->assertArrayHasKey('addr_city', $pet, 'Expect results to contain addr_city attributes');
        $this->assertArrayHasKey('addr_postal_code', $pet, 'Expect results to contain addr_postal_code attributes');
        $this->assertArrayHasKey('latitude', $pet, 'Expect results to contain latitude attributes');
        $this->assertArrayHasKey('longitude', $pet, 'Expect results to contain longitude attributes');
        $this->assertArrayHasKey('pet_details_url', $pet, 'Expect results to contain pet_details_url attributes');
    }

    /**
     * Assertions on the objects returned by pets method in
     * BulkExportService class using Pet Harbor's apiKey
     *
     * @return void
     */
    public function testPetsAsPetHarbor(): void
    {
        $this->testPets($this->petHarborApiKey);
    }

    /**
     * Assertions on the object returned by show method in
     * BulkExportService class
     *
     * @return void
     */
    public function testShelters($apiKey = null): void
    {
        $apiKey = $apiKey ?? $this->apiKey;
        $service = new BulkExportService($apiKey);
        $shelter = $service->shelters(['US', 'CA'], '2020-01-01 00:00:00', 10)[0]->getAttributes();
        $this->assertArrayHasKey('shelter_id', $shelter, 'Expect results to contain shelter_id attributes');
        $this->assertArrayHasKey('shelter_name', $shelter, 'Expect results to contain shelter_name attributes');
        $this->assertArrayHasKey('website_url', $shelter, 'Expect results to contain website_url attributes');
        $this->assertArrayHasKey('email', $shelter, 'Expect results to contain email attributes');
        $this->assertArrayHasKey('phone_area_code', $shelter, 'Expect results to contain phone_area_code attributes');
        $this->assertArrayHasKey('phone_number', $shelter, 'Expect results to contain phone_number attributes');
        $this->assertArrayHasKey('phone_extension', $shelter, 'Expect results to contain phone_extension attributes');
        $this->assertArrayHasKey('fax_area_code', $shelter, 'Expect results to contain fax_area_code attributes');
        $this->assertArrayHasKey('fax_number', $shelter, 'Expect results to contain fax_number attributes');
        $this->assertArrayHasKey('addr_line_1', $shelter, 'Expect results to contain addr_line_1 attributes');
        $this->assertArrayHasKey('addr_line_2', $shelter, 'Expect results to contain addr_line_2 attributes');
        $this->assertArrayHasKey('addr_city', $shelter, 'Expect results to contain addr_city attributes');
        $this->assertArrayHasKey('addr_state_code', $shelter, 'Expect results to contain addr_state_code attributes');
        $this->assertArrayHasKey('addr_postal_code', $shelter, 'Expect results to contain addr_postal_code attributes');
        $this->assertArrayHasKey('addr_country_code', $shelter, 'Expect results to contain addr_country_code attributes');
        $this->assertArrayHasKey('shelter_desc', $shelter, 'Expect results to contain shelter_desc attributes');
        $this->assertArrayHasKey('shelter_adoption_process', $shelter, 'Expect results to contain shelter_adoption_process attributes');
        $this->assertArrayHasKey('shelter_driving_dir', $shelter, 'Expect results to contain shelter_driving_dir attributes');
        $this->assertArrayHasKey('adopts_out_dogs_p', $shelter, 'Expect results to contain adopts_out_dogs_p attributes');
        $this->assertArrayHasKey('adopts_out_cats_p', $shelter, 'Expect results to contain adopts_out_cats_p attributes');
        $this->assertArrayHasKey('rescue_group_p', $shelter, 'Expect results to contain rescue_group_p attributes');
        $this->assertArrayHasKey('last_pet_added_timestamp', $shelter, 'Expect results to contain last_pet_added_timestamp attributes');
        $this->assertArrayHasKey('last_pet_updated_timestamp', $shelter, 'Expect results to contain last_pet_updated_timestamp attributes');
        $this->assertArrayHasKey('added_timestamp', $shelter, 'Expect results to contain added_timestamp attributes');
        $this->assertArrayHasKey('donation_html', $shelter, 'Expect results to contain donation_html attributes');
    }

    /**
     * Assertions on the objects returned by shelters method in
     * BulkExportService class using Pet Harbor's apiKey
     *
     * @return void
     */
    public function testSheltersAsPetHarbor(): void
    {
        $this->testShelters($this->petHarborApiKey);
    }
}

<?php

namespace Tests\Unit\V1;

use App\Http\Requests\V1\Npa\NpaSubscribeRequest;
use Illuminate\Validation\ValidationException;
use \Faker\Factory as FakerFactory;
use App\Models\V1\PetSearchQuery;
use App\Services\NpaService;
use Tests\TestCase;

class NpaSubscribeRequestTest extends TestCase
{
    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $faker = FakerFactory::create();
        $this->email = $faker->unique()->safeEmail;
        $this->query = factory(PetSearchQuery::class)->make()->toArray();
    }

    /**
     * Assertions on the errors returned by NpaSubscribeRequest for a request that's
     * missing required parameters
     *
     * @return void
     */
    public function testNpaSubscribeRequestMissingRequired(): void
    {
        $request = NpaSubscribeRequest::create(
            'npa/subscribe',
            'POST',
            [
                // ommitting key
                'email' => 'not_an_email_address',
                // inapprorpriate age, breed_id, color_id, and pet_size_range_id for specified species
                'search_params' => 'species=cat&age=puppy&breed_id=187&color_id=152&pet_size_range_id=4',
            ]
        );
        $this->expectException(ValidationException::class);
        try {
            $request->validate($request->rules());
        } catch (ValidationException $e) {
            $this->assertArrayHasKey('email', $e->errors(), 'errors include an email error');
            $this->assertArrayHasKey('key', $e->errors(), 'errors include a key error');
            $this->assertArrayHasKey('search_params.geo_range', $e->errors(), 'errors include a search_params.clan_id error');
            $this->assertArrayHasKey('search_params.city_state', $e->errors(), 'errors include a search_params.city_state error');
            $this->assertArrayHasKey('search_params.postal_code', $e->errors(), 'errors include a search_params.postal_code error');
            $this->assertArrayHasKey('search_params.age', $e->errors(), 'errors include a search_params.age error');
            $this->assertArrayHasKey('search_params.breed_id', $e->errors(), 'errors include a search_params.breed_id error');
            $this->assertArrayHasKey('search_params.color_id', $e->errors(), 'errors include a search_params.color_id error');
            $this->assertArrayHasKey('search_params.pet_size_range_id', $e->errors(), 'errors include a search_params.pet_size_range_id error');
            throw $e;
        }
    }
}

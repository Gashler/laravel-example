<?php

namespace Tests\Unit\V1;

use \Faker\Factory as FakerFactory;
use App\Exceptions\NpaSubscribeException;
use App\Models\V1\PetSearchQuery;
use App\Services\NpaService;
use Tests\TestCase;

class NpaServiceTest extends TestCase
{
    public $service;
    protected $query;

    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->service = new NpaService;
        $faker = FakerFactory::create();
        $this->email = $faker->unique()->safeEmail;
        $this->query = factory(PetSearchQuery::class)->make();
    }

    /**
     * Assertions on the basic instantiation of the NpaService
     *
     * @return void
     */
    public function testPetServiceInstance(): void
    {
        $this->assertInstanceOf(NpaService::class, $this->service, 'Expect service to be instance of App\Services\NpaService');
    }

    /**
     * Assertions on the object returned by show method in
     * NpaService class for Success case
     *
     * @return void
     */
    public function testSubscribeSuccess(): void
    {
        $result = $this->service->subscribe(
            $this->email,
            $this->query->toArray()
        );
        $this->assertObjectHasAttribute('post_new_pet_alert', $result, 'NPA service subscribe method subscribes a NPA');
    }

    /**
     * Assertions on the object returned by show method in
     * NpaService class for duplicate email case
     *
     * @return void
     */
    public function testPetQueryDuplicateEmail(): void
    {
        $this->expectException(NpaSubscribeException::class);
        $this->service->subscribe(
            'steve@adoptapet.com',
            $this->query->toArray()
        );
    }
}

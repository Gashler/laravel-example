<?php

namespace Tests\Unit\V1;

use App\Http\Requests\V1\Pets\PetDetailsRequest;
use App\Http\Requests\V1\Pets\PetSearchFormRequest;
use App\Http\Requests\V1\Pets\PetSearchRequest;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class PetRequestTest extends TestCase
{
    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp() : void
    {
        parent::setUp();
    }

    /**
     * Assertions on the errors returned by PetDetailsRequest for an
     * invalid request
     *
     * @return void
     */
    public function testPetDetailsRequestInvalid() : void
    {
        $request = PetDetailsRequest::create(
            '/search/pet_details',
            'GET',
            [
                'pet_id' => 'not_an_integer'
            ]
        );
        $this->expectException(ValidationException::class);
        try {
            $request->validate($request->rules());
        } catch (ValidationException $e) {
            $this->assertArrayHasKey('pet_id', $e->errors(), 'errors includes a pet_id error');
            throw $e;
        }
    }

    /**
     * Assertions on the errors returned by PetSearchFormRequest for an
     * invalid request
     *
     * @return void
     */
    public function testPetSearchFormRequestInvalid() : void
    {
        $request = PetSearchFormRequest::create(
            '/search/search_form',
            'GET',
            [
                'species' => 'zebra',
            ]
        );
        $this->expectException(ValidationException::class);
        try {
            $request->validate($request->rules());
        } catch (ValidationException $e) {
            $this->assertArrayHasKey('species', $e->errors(), 'errors includes a species error');
            throw $e;
        }
    }

    /**
     * Assertions on the errors returned by PetSearchRequest for an
     * invalid request
     *
     * @return void
     */
    public function testPetSearchRequestInvalid() : void
    {
        $request = PetSearchRequest::create(
            '/search/search',
            'GET',
            [
                // city_or_zip ommitted
                'geo_range' => 'not_an_integer',
                'species' => 'zebra',
            ]
        );
        $this->expectException(ValidationException::class);
        try {
            $request->validate($request->rules());
        } catch (ValidationException $e) {
            $this->assertArrayHasKey('city_or_zip', $e->errors(), 'errors includes a city_or_zip error');
            $this->assertArrayHasKey('geo_range', $e->errors(), 'errors includes a geo_range error');
            $this->assertArrayHasKey('species', $e->errors(), 'errors includes a species error');
            throw $e;
        }
    }
}

<?php

namespace Tests\Unit\V1;

use App\Http\Requests\V1\BulkExport\BulkExportRequestRequest;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class BulkExportRequestRequestTest extends TestCase
{
    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * Assertions on the errors returned by BulkExportRequestRequest for a
     * request that's missing required parameters
     *
     * @return void
     */
    public function testBulkExportRequestRequestMissingRequired(): void
    {
        $request = BulkExportRequestRequest::create(
            'npa/bulk-exports/pets',
            'GET',
            [
                // ommitting country
                // ommitting output
            ]
        );
        $this->expectException(ValidationException::class);
        try {
            $request->validate($request->rules());
        } catch (ValidationException $e) {
            $this->assertArrayHasKey('country', $e->errors(), 'errors include a country error');
            $this->assertArrayHasKey('output', $e->errors(), 'errors include an output error');
            throw $e;
        }
    }

    /**
     * Assertions on the errors returned by BulkExportRequestRequest for a
     * request with invalid parameters
     *
     * @return void
     */
    public function testBulkExportRequestRequestInvalid(): void
    {
        $request = BulkExportRequestRequest::create(
            'npa/bulk-exports/pets',
            'GET',
            [
                'country' => 'XY', // invalid country
                'output' => 'JSON', // invalid output
                'added_after' => '2020-06-09T17:49:23' // incorrectly including seconds
            ]
        );
        $this->expectException(ValidationException::class);
        try {
            $request->validate($request->rules());
        } catch (ValidationException $e) {
            $this->assertArrayHasKey('country', $e->errors(), 'errors include a country error');
            $this->assertArrayHasKey('output', $e->errors(), 'errors include an output error');
            $this->assertArrayHasKey('added_after', $e->errors(), 'errors include an output error');
            throw $e;
        }
    }
}

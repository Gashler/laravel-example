<?php

namespace Tests\Unit\V1;

use App\Http\Requests\V1\Shelters\ShelterDetailsRequest;
use App\Http\Requests\V1\Shelters\ShelterDetailsWithPetsRequest;
use App\Http\Requests\V1\Shelters\ShelterSearchRequest;
use App\Http\Requests\V1\Shelters\ShelterIndexWithPetsRequest;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class ShelterRequestTest extends TestCase
{
    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp() : void
    {
        parent::setUp();
    }

    /**
     * Assertions on the errors returned by ShelterDetailsRequest for an
     * invalid request
     *
     * @return void
     */
    public function testShelterDetailsRequestInvalid() : void
    {
        $request = ShelterDetailsRequest::create(
            '/search/shelter_details',
            'GET',
            [
                'shelter_id' => 'not_an_integer'
            ]
        );
        $this->expectException(ValidationException::class);
        try {
            $request->validate($request->rules());
        } catch (ValidationException $e) {
            $this->assertArrayHasKey('shelter_id', $e->errors(), 'errors includes a shelter_id error');
            throw $e;
        }
    }

    /**
     * Assertions on the errors returned by ShelterDetailsWithPetsRequest for an
     * invalid request
     *
     * @return void
     */
    public function testShelterDetailsWithPetsRequestInvalid() : void
    {
        $request = ShelterDetailsWithPetsRequest::create(
            '/search/shelter_details',
            'GET',
            [
                'shelter_id' => 'not_an_integer'
            ]
        );
        $this->expectException(ValidationException::class);
        try {
            $request->validate($request->rules());
        } catch (ValidationException $e) {
            $this->assertArrayHasKey('shelter_id', $e->errors(), 'errors includes a shelter_id error');
            throw $e;
        }
    }

    /**
     * Assertions on the errors returned by ShelterSearchRequest for an
     * invalid request
     *
     * @return void
     */
    public function testShelterSearchRequestInvalid() : void
    {
        $request = ShelterSearchRequest::create(
            '/search/search',
            'GET',
            [
                // city_or_zip ommitted
                'geo_range' => 'not_an_integer',
            ]
        );
        $this->expectException(ValidationException::class);
        try {
            $request->validate($request->rules());
        } catch (ValidationException $e) {
            $this->assertArrayHasKey('city_or_zip', $e->errors(), 'errors includes a city_or_zip error');
            $this->assertArrayHasKey('geo_range', $e->errors(), 'errors includes a geo_range error');
            throw $e;
        }
    }

    /**
     * Assertions on the errors returned by ShelterIndexWithPetsRequest for an
     * invalid request
     *
     * @return void
     */
    public function testShelterIndexWithPetsRequestInvalid() : void
    {
        $request = ShelterIndexWithPetsRequest::create(
            '/search/search',
            'GET',
            [
                'shelter_ids' => 'not_an_array',
            ]
        );
        $this->expectException(ValidationException::class);
        try {
            $request->validate($request->rules());
        } catch (ValidationException $e) {
            $this->assertArrayHasKey('shelter_ids', $e->errors(), 'errors includes a shelter_ids error');
            throw $e;
        }
    }
}

<?php

namespace Tests\Unit\V1;

use Tests\TestCase;
use App\Repositories\ShelterRepository;
use App\Models\V1\Pet;
use App\Models\V1\Shelter;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ShelterRepositoryTest extends TestCase
{
    public $repo;
    protected $params;

    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp() : void
    {
        parent::setUp();
        $this->repo = new ShelterRepository;
        $this->params = [
            'city_or_zip' => 84606,
            'geo_range' => 50,
            'shelter_id' => 98957,
            'shelter_ids' => [98957, 94752],
            'invalid_string' => '00000',
        ];
    }

    /**
     * Assertions on the basic instantiation of the ShelterRepository
     *
     * @return void
     */
    public function testRepositoryInstance() : void
    {
        $this->assertInstanceOf(ShelterRepository::class, $this->repo, 'Expect repository to be instance of App\Repositories\ShelterRepository');
    }

    /**
     * Assertions on the object returned by show method in
     * ShelterRepository class for Success case
     *
     * @return void
     */
    public function testShowSuccess() : void
    {
        $result = $this->repo->show(
            $this->params['shelter_id'],
            ['pets']
        );
        $this->assertInstanceOf(Shelter::class, $result, 'result is a Shelter model instance');
        $this->assertEquals($this->params['shelter_id'], $result->shelter_id, 'Shelter model instance shelter_id matches the provided id argument');
        $this->assertInstanceOf(Pet::class, $result->pets[0], 'result contains Pet model instances');
    }

    /**
     * Assertions on the object returned by show method in
     * ShelterRepository class for Not Found case
     *
     * @return void
     */
    public function testShowNotFound() : void
    {
        $this->expectException(ModelNotFoundException::class);
        $result = $this->repo->show($this->params['invalid_string']);
    }

    /**
     * Assertions on the objects returned by search method in
     * ShelterRepository class for Success case
     *
     * @return void
     */
    public function testSearch() : void
    {
        $result = $this->repo->search(
            [
                'geo_range' => $this->params['geo_range'],
                'city_or_zip' => $this->params['city_or_zip'],
                'adopts_out' => true,
                'with' => ['pets']
            ]
        );
        $this->assertInstanceOf(Shelter::class, $result[0], 'response body contains Shelter model instances');
        $this->assertInstanceOf(Pet::class, $result[0]->pets[0], 'response body contains Pet model instances');
    }

    /**
     * Assertions on the objects returned by search method using ids in
     * ShelterRepository class for Success case
     *
     * @return void
     */
    public function testSearchWithIds() : void
    {
        $result = $this->repo->index(
            [
                'ids' => $this->params['shelter_ids'],
                'with' => ['pets']
            ]
        );
        $this->assertInstanceOf(Shelter::class, $result[0], 'response body contains Shelter model instances');
        $this->assertInstanceOf(Pet::class, $result[0]->pets[0], 'response body contains Pet model instances');
    }
}

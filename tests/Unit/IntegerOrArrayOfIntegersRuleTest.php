<?php

namespace Tests\Unit\V1;

use App\Rules\IntegerOrArrayOfIntegers;
use Tests\TestCase;

class IntegerOrArrayofIntegersRuleTest extends TestCase
{
    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->rule = new IntegerOrArrayOfIntegers;
    }

    /**
     * Assertions on the basic instantiation of the IntegerOrArrayOfIntegers class
     *
     * @return void
     */
    public function testHandlerInstance(): void
    {
        $this->assertInstanceOf(IntegerOrArrayOfIntegers::class, $this->rule, 'Expect rule to be instance of App\Rules\IntegerOrArrayOfIntegers');
    }

    /**
     * Assertions on the response returned by the passes method for the IntegerOrArrayOfIntegers
     * class for a success state for a single integer
     *
     * @return void
     */
    public function testPassesIntegerSuccess(): void
    {
        $result = $this->rule->passes(
            'some_key',
            12345
        );
        $this->assertNotFalse($result, 'Result returns true for a properly formatted integer');
    }

    /**
     * Assertions on the response returned by the passes method for the IntegerOrArrayOfIntegers
     * class for a success state for a an array of integers
     *
     * @return void
     */
    public function testPassesArraySuccess(): void
    {
        $result = $this->rule->passes(
            'some_key',
            [123, 456]
        );
        $this->assertNotFalse($result, 'Result returns true for a properly formatted array');
    }

    /**
     * Assertions on the response returned by the passes method for the IntegerOrArrayOfIntegers
     * class for an invalid state for a single integer
     *
     * @return void
     */
    public function testPassesIntegerInvalid(): void
    {
        $result = $this->rule->passes(
            'some_key',
            'abc123' // not an integer
        );
        $this->assertFalse($result, 'Result returns false for an improperly formatted integer');
    }

    /**
     * Assertions on the response returned by the passes method for the IntegerOrArrayOfIntegers
     * class for an invalid state for an array of integers
     *
     * @return void
     */
    public function testPassesArrayInvalid(): void
    {
        $result = $this->rule->passes(
            'some_key',
            [123, 'abc'] // not all integers
        );
        $this->assertFalse($result, 'Result returns false for an improperly formatted array');
    }
}

<?php

namespace Tests\Unit\V2;

use App\Http\Requests\V2\BulkExport\BulkExportRequestRequest;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class BulkExportRequestRequestTest extends TestCase
{
    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * Assertions on the errors returned by BulkExportRequestRequest for a
     * request that's missing required parameters
     *
     * @return void
     */
    public function testBulkExportRequestRequestMissingRequired(): void
    {
        $request = BulkExportRequestRequest::create(
            'npa/bulk-exports/pets',
            'GET',
            [
                // ommitting country
                // ommitting output (not required)
                'since_ts' => time() - 86400,
                'until_ts' => time(),
                'orgID' => 123,
            ]
        );
        $this->expectException(ValidationException::class);
        try {
            $request->validate($request->rules());
        } catch (ValidationException $e) {
            $this->assertArrayHasKey('country', $e->errors(), 'errors include a country error');
            $this->assertArrayNotHasKey('output', $e->errors(), 'errors include an output error');
            $this->assertArrayNotHasKey('since_ts', $e->errors(), 'errors do not include as since_ts error');
            $this->assertArrayNotHasKey('until_ts', $e->errors(), 'errors do not include an until_ts error');
            $this->assertArrayNotHasKey('orgID', $e->errors(), 'errors do not include an orgID error');
            throw $e;
        }
    }

    /**
     * Assertions on the errors returned by BulkExportRequestRequest for a
     * request with invalid parameters
     *
     * @return void
     */
    public function testBulkExportRequestRequestInvalid(): void
    {
        $request = BulkExportRequestRequest::create(
            'npa/bulk-exports/pets',
            'GET',
            [
                'country' => 'XY', // invalid country
                'output' => 'CSV', // invalid output
                'since_ts' => '2020-06-01 00:00:00', // timestamp instead of UNIX time
                'until_ts' => '2020-06-05 00:00:00', // timestamp instead of UNIX time
                'orgID' => ['abc', 'def'] // strings instead of integers
            ]
        );
        $this->expectException(ValidationException::class);
        try {
            $request->validate($request->rules());
        } catch (ValidationException $e) {
            $this->assertArrayHasKey('country', $e->errors(), 'errors include a country error');
            $this->assertArrayHasKey('output', $e->errors(), 'errors include an output error');
            $this->assertArrayHasKey('since_ts', $e->errors(), 'errors include an since_ts error');
            $this->assertArrayHasKey('until_ts', $e->errors(), 'errors include an until_ts error');
            $this->assertArrayHasKey('orgID', $e->errors(), 'errors include an orgID error');
            throw $e;
        }
    }
}

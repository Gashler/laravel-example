<?php

namespace Tests\Unit\V2;

use App\Models\V2\BulkExportJob;
use App\Models\V2\BulkExportPet;
use App\Jobs\V2\BulkExport\BulkExportPetsJob;
use App\Models\V1\Partner;
use Illuminate\Support\Collection;
use Tests\TestCaseWithApiKey;

class BulkExportPetsJobTest extends TestCaseWithApiKey
{
    protected $pet;

    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->pet = factory(BulkExportPet::class)->make();
        $bulkExportJob = factory(BulkExportJob::class)->make(['partner_id' => $this->partnerId]);
        $this->job = new BulkExportPetsJob($bulkExportJob, 10);
    }

    /**
     * Assertions on the objects returned by hanle method in
     * BulkExportPetsJob class
     *
     * @return void
     */
    public function testHandle(
        array $params = null,
        int $limit = 1,
        bool $verbose = true
    ): void {
        $params = $params ?? ['delta_type' => null];
        $params['entity'] = 'pets';
        $bulkExportJob = factory(BulkExportJob::class)->make($params);
        $bulkExportJob->save();
        $bulkExportPetsJob = new BulkExportPetsJob(
            $bulkExportJob,
            $limit,
            $verbose
        );

        $results = $bulkExportPetsJob->handle();
        $this->assertInstanceOf(Collection::class, $results, 'Expect results to be an array');
        if (count($results) > 0) {
            $pet = $results[0];

            // Force accessors to generate for testing (this will happen automatically when the
            // data is converted to JSON.
            $pet = json_decode($pet->toJson());
            $pet->pictures = json_decode($pet->pictures);

            $this->assertObjectHasAttribute('oKWithFarmAnimals', $pet, 'Expect results to contain an oKWithFarmAnimals parameter');
            $this->assertObjectHasAttribute('videoUrls', $pet, 'Expect results to contain a videoUrls parameter');
            $this->assertObjectHasAttribute('tailType', $pet, 'Expect results to contain a tailType parameter');
            $this->assertObjectHasAttribute('secondaryBreed', $pet, 'Expect results to contain a secondaryBreeda parameter');
            $this->assertObjectHasAttribute('groomingNeeds', $pet, 'Expect results to contain a groomingNeeds parameter');
            $this->assertObjectHasAttribute('sightImpaired', $pet, 'Expect results to contain a sightImpaired parameter');
            $this->assertObjectHasAttribute('swims', $pet, 'Expect results to contain a swims parameter');
            $this->assertObjectHasAttribute('orgID', $pet, 'Expect results to contain a orgID parameter');
            $this->assertObjectHasAttribute('foundDate', $pet, 'Expect results to contain a foundDate parameter');
            $this->assertObjectHasAttribute('messagePet', $pet, 'Expect results to contain a messagePet parameter');
            $this->assertObjectHasAttribute('noLargeDogs', $pet, 'Expect results to contain a noLargeDogs parameter');
            $this->assertObjectHasAttribute('housetrained', $pet, 'Expect results to contain a housetrained parameter');
            $this->assertObjectHasAttribute('shedding', $pet, 'Expect results to contain a shedding parameter');
            $this->assertObjectHasAttribute('specialNeeds', $pet, 'Expect results to contain a specialNeeds parameter');
            $this->assertObjectHasAttribute('species', $pet, 'Expect results to contain a species parameter');
            $this->assertObjectHasAttribute('kids', $pet, 'Expect results to contain a kids parameter');
            $this->assertObjectHasAttribute('fetches', $pet, 'Expect results to contain a fetches parameter');
            $this->assertObjectHasAttribute('sex', $pet, 'Expect results to contain a sex parameter');
            $this->assertObjectHasAttribute('cratetrained', $pet, 'Expect results to contain a cratetrained parameter');
            $this->assertObjectHasAttribute('protective', $pet, 'Expect results to contain a protective parameter');
            $this->assertObjectHasAttribute('animalLocation', $pet, 'Expect results to contain a animalLocation parameter');
            $this->assertObjectHasAttribute('sizeUOM', $pet, 'Expect results to contain a sizeUOM parameter');
            $this->assertObjectHasAttribute('status', $pet, 'Expect results to contain a status parameter');
            $this->assertObjectHasAttribute('exerciseNeeds', $pet, 'Expect results to contain a exerciseNeeds parameter');
            $this->assertObjectHasAttribute('leashtrained', $pet, 'Expect results to contain a leashtrained parameter');
            $this->assertObjectHasAttribute('newPeople', $pet, 'Expect results to contain a newPeople parameter');
            $this->assertObjectHasAttribute('breed', $pet, 'Expect results to contain a breed parameter');
            $this->assertObjectHasAttribute('hearingImpaired', $pet, 'Expect results to contain a hearingImpaired parameter');
            $this->assertObjectHasAttribute('skittish', $pet, 'Expect results to contain a skittish parameter');
            $this->assertObjectHasAttribute('fence', $pet, 'Expect results to contain a fence parameter');
            $this->assertObjectHasAttribute('specialDiet', $pet, 'Expect results to contain a specialDiet parameter');
            $this->assertObjectHasAttribute('contactName', $pet, 'Expect results to contain a contactName parameter');
            $this->assertObjectHasAttribute('hypoallergenic', $pet, 'Expect results to contain a hypoallergenic parameter');
            $this->assertObjectHasAttribute('altered', $pet, 'Expect results to contain a altered parameter');
            $this->assertObjectHasAttribute('found', $pet, 'Expect results to contain a found parameter');
            $this->assertObjectHasAttribute('yardRequired', $pet, 'Expect results to contain a yardRequired parameter');
            $this->assertObjectHasAttribute('primaryBreed', $pet, 'Expect results to contain a primaryBreed parameter');
            $this->assertObjectHasAttribute('escapes', $pet, 'Expect results to contain a escapes parameter');
            $this->assertObjectHasAttribute('apartment', $pet, 'Expect results to contain a apartment parameter');
            $this->assertObjectHasAttribute('goofy', $pet, 'Expect results to contain a goofy parameter');
            $this->assertObjectHasAttribute('pattern', $pet, 'Expect results to contain a pattern parameter');
            $this->assertObjectHasAttribute('age', $pet, 'Expect results to contain a age parameter');
            $this->assertObjectHasAttribute('mediaLastUpdated', $pet, 'Expect results to contain a mediaLastUpdated parameter');
            $this->assertObjectHasAttribute('activityLevel', $pet, 'Expect results to contain a activityLevel parameter');
            $this->assertObjectHasAttribute('independent', $pet, 'Expect results to contain a independent parameter');
            $this->assertObjectHasAttribute('adoptionFee', $pet, 'Expect results to contain a adoptionFee parameter');
            $this->assertObjectHasAttribute('foundZipcode', $pet, 'Expect results to contain a foundZipcode parameter');
            $this->assertObjectHasAttribute('mixed', $pet, 'Expect results to contain a mixed parameter');
            $this->assertObjectHasAttribute('playsToys', $pet, 'Expect results to contain a playsToys parameter');
            $this->assertObjectHasAttribute('olderKidsOnly', $pet, 'Expect results to contain a olderKidsOnly parameter');
            $this->assertObjectHasAttribute('obedient', $pet, 'Expect results to contain a obedient parameter');
            $this->assertObjectHasAttribute('killDate', $pet, 'Expect results to contain a killDate parameter');
            $this->assertObjectHasAttribute('energyLevel', $pet, 'Expect results to contain a energyLevel parameter');
            $this->assertObjectHasAttribute('coatLength', $pet, 'Expect results to contain a coatLength parameter');
            $this->assertObjectHasAttribute('description', $pet, 'Expect results to contain a description parameter');
            $this->assertObjectHasAttribute('killReason', $pet, 'Expect results to contain a killReason parameter');
            $this->assertObjectHasAttribute('timid', $pet, 'Expect results to contain a timid parameter');
            $this->assertObjectHasAttribute('earType', $pet, 'Expect results to contain a earType parameter');
            $this->assertObjectHasAttribute('pictures', $pet, 'Expect results to contain a pictures parameter');
            $this->assertObjectHasAttribute('eventempered', $pet, 'Expect results to contain a eventempered parameter');
            $this->assertObjectHasAttribute('sizeCurrent', $pet, 'Expect results to contain a sizeCurrent parameter');
            $this->assertObjectHasAttribute('dogs', $pet, 'Expect results to contain a dogs parameter');
            $this->assertObjectHasAttribute('eagerToPlease', $pet, 'Expect results to contain a eagerToPlease parameter');
            $this->assertObjectHasAttribute('needsFoster', $pet, 'Expect results to contain a needsFoster parameter');
            $this->assertObjectHasAttribute('cats', $pet, 'Expect results to contain a cats parameter');
            $this->assertObjectHasAttribute('eyeColor', $pet, 'Expect results to contain a eyeColor parameter');
            $this->assertObjectHasAttribute('noHeat', $pet, 'Expect results to contain a noHeat parameter');
            $this->assertObjectHasAttribute('predatory', $pet, 'Expect results to contain a predatory parameter');
            $this->assertObjectHasAttribute('oKForSeniors', $pet, 'Expect results to contain a oKForSeniors parameter');
            $this->assertObjectHasAttribute('ongoingMedical', $pet, 'Expect results to contain a ongoingMedical parameter');
            $this->assertObjectHasAttribute('uptodate', $pet, 'Expect results to contain a uptodate parameter');
            $this->assertObjectHasAttribute('affectionate', $pet, 'Expect results to contain a affectionate parameter');
            $this->assertObjectHasAttribute('courtesy', $pet, 'Expect results to contain a courtesy parameter');
            if ($pet->contactPhone !== null) {
                $this->assertIsString($pet->contactPhone, 'Expect results to contain a contactPhone parameter');
            }
            $this->assertObjectHasAttribute('ownerExperience', $pet, 'Expect results to contain a ownerExperience parameter');
            $this->assertObjectHasAttribute('playful', $pet, 'Expect results to contain a playful parameter');
            $this->assertObjectHasAttribute('animalID', $pet, 'Expect results to contain a animalID parameter');
            $this->assertObjectHasAttribute('size', $pet, 'Expect results to contain a size parameter');
            $this->assertObjectHasAttribute('obedienceTraining', $pet, 'Expect results to contain a obedienceTraining parameter');
            $this->assertObjectHasAttribute('oKWithAdults', $pet, 'Expect results to contain a oKWithAdults parameter');
            $this->assertObjectHasAttribute('contactEmail', $pet, 'Expect results to contain a contactEmail parameter');
            $this->assertObjectHasAttribute('color', $pet, 'Expect results to contain a color parameter');
            $this->assertObjectHasAttribute('noCold', $pet, 'Expect results to contain a noCold parameter');
            $this->assertObjectHasAttribute('intelligent', $pet, 'Expect results to contain a intelligent parameter');
            $this->assertObjectHasAttribute('vocal', $pet, 'Expect results to contain a vocal parameter');
            $this->assertObjectHasAttribute('declawed', $pet, 'Expect results to contain a declawed parameter');
            $this->assertObjectHasAttribute('goodInCar', $pet, 'Expect results to contain a goodInCar parameter');
            $this->assertObjectHasAttribute('noFemaleDogs', $pet, 'Expect results to contain a noFemaleDogs parameter');
            $this->assertObjectHasAttribute('noMaleDogs', $pet, 'Expect results to contain a noMaleDogs parameter');
            $this->assertObjectHasAttribute('name', $pet, 'Expect results to contain a name parameter');
            $this->assertObjectHasAttribute('hasAllergies', $pet, 'Expect results to contain a hasAllergies parameter');
            $this->assertObjectHasAttribute('lap', $pet, 'Expect results to contain a lap parameter');
            $this->assertObjectHasAttribute('gentle', $pet, 'Expect results to contain a gentle parameter');
            $this->assertObjectHasAttribute('rescueID', $pet, 'Expect results to contain a rescueID parameter');
            $this->assertObjectHasAttribute('drools', $pet, 'Expect results to contain a drools parameter');
            $this->assertObjectHasAttribute('noSmallDogs', $pet, 'Expect results to contain a noSmallDogs parameter');
            $this->assertObjectHasAttribute('lastUpdated', $pet, 'Expect results to contain a lastUpdated parameter');
        }
    }

    /**
     * Assertions on the objects returned by hanle method in
     * BulkExportPetsJob class for the added delta type
     *
     * @return void
     */
    public function testHandleAdded(): void
    {
        $this->testHandle(['delta_type' => 'added']);
    }

    /**
     * Assertions on the objects returned by hanle method in
     * BulkExportPetsJob class for the updated delta type
     *
     * @return void
     */
    public function testHandleUpdated(): void
    {
        $this->testHandle(['delta_type' => 'updated']);
    }

    /**
     * Assertions on the objects returned by hanle method in
     * BulkExportPetsJob class for the deleted delta type
     *
     * @return void
     */
    public function testHandleDeleted(): void
    {
        $this->testHandle(
            ['delta_type' => 'deleted'],
            1,
            true // also test logging
        );
    }

    /**
     * Test the special conditions for disaster-recovery apps for Hurricane
     * Harvey & Irma
     *
     * @return void
     */
    public function testHandleDisasterRecoveryApps(): void
    {
        $this->testHandle(
            [
                'partner_id' => Partner::where('tech_name', 'finding_rover')
                    ->first()
                    ->partner_id
            ]
        );
    }
}

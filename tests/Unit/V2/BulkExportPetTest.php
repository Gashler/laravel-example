<?php

namespace Tests\Unit\V2;

use App\Models\V2\BulkExportPet;
use DB;
use Tests\TestCaseWithApiKey;

class BulkExportPetTest extends TestCaseWithApiKey
{
    protected $pet;

    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->pet = factory(BulkExportPet::class)->make();
    }

    /**
     * Assertions on the object returned by getContactNameAttribute method in
     * BulkExportPet class
     *
     * @return void
     */
    public function testGetContactNameAttribute(): void
    {
        $this->assertIsString($this->pet->contactName, 'Expect results to contain a formatted contactName attribute');
    }

    /**
     * Assertions on the object returned by fixContactPhone method in
     * BulkExportPet class
     *
     * @return void
     */
    public function testGetContactPhoneAttribute(): void
    {
        $this->assertIsString($this->pet->contactPhone, 'Expect results to contain a formatted contactName attribute');
    }

    /**
     * Assertions on the object returned by getVideoUrlsAttribute method in
     * BulkExportPet class
     *
     * @return void
     */
    public function testGetVideoUrlsAttribute(): void
    {
        $this->assertIsArray($this->pet->videoUrls, 'Expect results to contain a formatted videoUrls attribute');
    }

    /**
     * Assertions on the object returned by getBreedAttribute method in
     * BulkExportPet class
     *
     * @return void
     */
    public function testGetBreedAttribute(): void
    {
        $this->assertIsString($this->pet->breed, 'Expect results to contain a formatted breed attribute');
    }

    /**
     * Assertions on the object returned by genPhotosStructure method in
     * BulkExportPet class
     *
     * @return void
     */
    public function testGetPicturesAttribute(): void
    {
        $photo = DB::table('photos')
            ->where(['entity_type' => 'pets', 'tag' => 'parent'])
            ->first();
        $pet = BulkExportPet::find($photo->entity_id);
        $pet->animalID = $pet->pet_id;
        $pictures = json_decode($pet->pictures);
        $this->assertObjectHasAttribute('originalUrl', $pictures[0], 'Expect results to contain an array of pictures');
        $this->assertObjectHasAttribute('thumbnailUrl', $pictures[0], 'Expect results to contain a thumbnailUrl parameter');
        $this->assertObjectHasAttribute('fullsizeUrl', $pictures[0], 'Expect results to contain a fullsizeUrl parameter');
        $this->assertObjectHasAttribute('originalUrl', $pictures[0], 'Expect results to contain a originalUrl parameter');
        $this->assertObjectHasAttribute('mediaOrder', $pictures[0], 'Expect results to contain a mediaOrder parameter');
        $this->assertObjectHasAttribute('largeUrl', $pictures[0], 'Expect results to contain a largeUrl parameter');
        $this->assertObjectHasAttribute('lastUpdated', $pictures[0], 'Expect results to contain a lastUpdated parameter');
        $this->assertObjectHasAttribute('smallUrl', $pictures[0], 'Expect results to contain a smallUrl parameter');
    }

    /**
     * Assertions on the object returned by getImageLocation method in
     * BulkExportPet class
     *
     * @return void
     */
    public function testGetImageLocation(): void
    {
        $location = $this->pet->getImageLocation(12345, 'jpg');
        $this->assertIsString($location, 'Expect results to contain a formatted formatted image location attribute');
    }

    /**
     * Assertions on the object returned by fixBoolFields method in
     * BulkExportPet class
     *
     * @return void
     */
    public function testFixBoolFields(): void
    {
        $this->pet->specialNeeds = true;
        $pet = $this->pet->fixBoolFields();
        $this->assertEquals('Yes', $pet->specialNeeds, 'Expect results to contain formatted boolean attributes');
    }
}

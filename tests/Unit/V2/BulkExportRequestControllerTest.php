<?php

namespace Tests\Unit\V2;

use App\Http\Controllers\V2\BulkExport\BulkExportRequestController;
use App\Http\Requests\V2\BulkExport\BulkExportRequestRequest;
use Tests\TestCaseWithApiKey;

class BulkExportRequestControllerTest extends TestCaseWithApiKey
{
    public $controller;

    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->controller = new BulkExportRequestController;
    }

    /**
     * Assertions on the basic instantiation of the V2 BulkExportRequestController
     *
     * @return void
     */
    public function testControllerInstance(): void
    {
        $this->assertInstanceOf(
            BulkExportRequestController::class,
            $this->controller,
            'Expect controller to be instance of App\Http\Controllers\V2\BulkExport\BulkExportRequestController'
        );
    }

    /**
     * Assertions on the response object returned by pets method in
     * V2\BulkExportRequestController class for Success case
     *
     * @return void
     */
    public function testPetsSuccess(): void
    {
        $request = BulkExportRequestRequest::create(
            '/bulk-export/pets',
            'GET',
            [
                'key' => $this->apiKey,
                'country' => ['US', 'CA'],
                'output' => 'csv',
                'limit' => 2
            ]
        );
        $response = $this->controller->pets($request);
        $this->assertEquals(200, $response->getStatusCode(), 'pets method returns a 200 response');
        $content = $response->getOriginalContent();
        $this->assertArrayHasKey('status', $content, 'pets method response contains a status attribute');
        $this->assertArrayHasKey('location', $content, 'pets method response contains a location attribute');
        $this->assertArrayHasKey('status_token', $content, 'pets method response contains a status_token attribute');
    }

    /**
     * Assertions on the response object returned by shelters method in
     * V2\BulkExportRequestController class for Success case
     *
     * @return void
     */
    public function testSheltersSuccess(): void
    {
        $request = BulkExportRequestRequest::create(
            '/bulk-export/shelters',
            'GET',
            [
                'key' => $this->apiKey,
                'country' => ['US'],
                'output' => 'csv',
                'limit' => 2
            ]
        );
        $response = $this->controller->shelters($request);
        $this->assertEquals(200, $response->getStatusCode(), 'shelters returns a 200 response');
        $content = $response->getOriginalContent();
        $this->assertArrayHasKey('status', $content, 'shelters method response contains a status attribute');
        $this->assertArrayHasKey('location', $content, 'shelters method response contains a location attribute');
        $this->assertArrayHasKey('status_token', $content, 'shelters method response contains a status_token attribute');
    }
}

<?php

namespace Tests\Unit\V2;

use App\Exceptions\V2\BulkExport\BulkExportDownloadNotFound;
use App\Exceptions\V2\BulkExport\BulkExportDownloadNotReady;
use App\Exceptions\V2\BulkExport\BulkExportStatusNotFound;
use App\Models\V1\Shelter;
use App\Services\V2\BulkExportService;
use Tests\TestCaseWithApiKey;

class BulkExportServiceTest extends TestCaseWithApiKey
{
    protected $service;
    protected $statusToken;

    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->service = new BulkExportService($this->apiKey);
    }

    /**
     * Assertions on the basic instantiation of the BulkExportService
     *
     * @return void
     */
    public function testPetServiceInstance(): void
    {
        $this->assertInstanceOf(BulkExportService::class, $this->service, 'Expect service to be instance of App\Services\V2\BulkExportService');
    }

    /**
     * Assertions on the array returned by handlePetsExport method in
     * BulkExportService class
     *
     * @return void
     */
    public function testhandlePetsExport(): void
    {
        $randomShelterIds = Shelter::select('shelter_id')
            ->inRandomOrder()
            ->limit(10)
            ->get()
            ->pluck('shelter_id')
            ->toArray();

        $result = $this->service->handlePetsExport(
            [
                'org_ids' => $randomShelterIds,
                'since_ts' => strtotime('2020-04-01 00:00:00'),
                'until_ts' => strtotime('2020-05-30 00:00:00'),
                'exclude_software' => 'rescuegroups',
                'limit' => 10,
                'country' => 'US',
                'delta_type' => 'all_pets',
                'options' => 'detailsUrl'
            ]
        );

        $this->assertIsString($result['location'], 'Expect results to contain a location string');
        $this->assertIsString($result['status_token'], 'Expect results to contain a token string');
    }

    /**
     * Assertions on the array returned by handleSheltersExport method in
     * BulkExportService class
     *
     * @return void
     */
    public function testHandleSheltersExport(): void
    {
        $result = $this->service->handleSheltersExport(
            [
                'since_ts' => strtotime('2020-04-01 00:00:00'),
                'until_ts' => strtotime('2020-05-30 00:00:00'),
                'exclude_software' => 'rescuegroups',
                'limit' => 10,
                'country' => 'US',
                'delta_type' => 'all_shelters'
            ]
        );

        $this->assertIsString($result['location'], 'Expect results to contain a location string');
        $this->assertIsString($result['status_token'], 'Expect results to contain a token string');
        $this->statusToken = $result['status_token'];
    }

    /**
     * Assertions on the data returned by status method in
     * BulkExportService class for the not found state
     *
     * @return void
     */
    public function testStatusNotFound(): void
    {
        $this->expectException(BulkExportStatusNotFound::class);
        $statusToken = $this->statusToken ?? 'abc123'; // a bad status token
        $result = $this->service->getStatus($statusToken);
        $this->assertEquals(404, $result->code, 'Expect result to return a 404 error');
    }

    /**
     * Assertions on the data returned by status method in
     * BulkExportService class for the pending state
     *
     * @return void
     */
    public function testStatusPending(): void
    {
        $statusToken = $this->statusToken ?? 'fd7f12c58cd7fba29f6748139d6432b9'; // a pending job
        $result = $this->service->getStatus($statusToken);
        $this->assertEquals('pending', $result['status'], 'Expect status to be "pending" for an incomplete job');
    }

    /**
     * Assertions on the object returned by status method in
     * BulkExportService class for the pending state
     *
     * @return void
     */
    public function testStatusCompleted(): void
    {
        $statusToken = 'f02476d773bef84a001f4c7ded2b1e53'; // a completed job
        $result = $this->service->getStatus($statusToken);
        $this->assertEquals('completed', $result->status, 'Expect status to be "completed" for a completed job');
        $this->assertIsString($result->download_token, 'Expect results to include download_token');
        $this->assertIsString($result->download_url, 'Expect results to include download_url');
    }

    /**
     * Assertions on the response returned by download method in
     * BulkExportService class for the not found state
     *
     * @return void
     */
    public function testDownloadNotFound(): void
    {
        $this->expectException(BulkExportDownloadNotFound::class);
        $downloadToken = 'abc123'; // bad download token
        $result = $this->service->download($downloadToken);
        $this->assertEquals(404, $result->code, 'Expect result to return a 404 error');
    }

    /**
     * Assertions on the response returned by download method in
     * BulkExportService class for the not ready state
     *
     * @return void
     */
    public function testDownloadNotReady(): void
    {
        $this->expectException(BulkExportDownloadNotReady::class);
        $downloadToken = '9d3d1243adff086d72cce81898762a8d'; // pending download token
        $result = $this->service->download($downloadToken);
        $this->assertEquals(500, $result->code, 'Expect result to return a 500 error');
    }

    /**
     * Assertions on the data returned by download method in
     * BulkExportService class for the success state
     *
     * @return void
     */
    public function testDownloadSuccess(): void
    {
        $downloadToken = 'b70dd380e32081e0f91ac863c8591934'; // completed download token
        $result = $this->service->download($downloadToken);
        $this->assertIsString($result, 'Expect result to be a download URL');
    }
}

<?php

namespace Tests\Unit\V2;

use App\Models\V2\BulkExportJob;
use App\Jobs\V2\BulkExport\BulkExportSheltersJob;
use App\Models\V1\Partner;
use App\Models\V1\Shelter;
use DB;
use Illuminate\Support\Collection;
use Tests\TestCaseWithApiKey;

class BulkExportSheltersJobTest extends TestCaseWithApiKey
{
    /**
     * Assertions on the objects returned by hanle method in
     * BulkExportSheltersJob class
     *
     * @return void
     */
    public function testHandle(
        array $params = null,
        int $limit = 10,
        bool $verbose = false
    ): void {
        $params = $params ?? [
            'entity' => 'shelters',
            'delta_type' => null
        ];
        $bulkExportJob = factory(BulkExportJob::class)->make($params);
        $bulkExportJob->save();
        $bulkExportSheltersJob = new BulkExportSheltersJob(
            $bulkExportJob,
            $limit,
            $verbose
        );
        $results = $bulkExportSheltersJob->handle();
        $this->assertInstanceOf(Collection::class, $results, 'Expect results to be an array');
        if (count($results) > 0) {
            $this->assertIsInt($results[0]->orgid, 'Expect results to contain a shelter_id string');
        }
    }

    /**
     * Test handle for 'added' delta_type
     *
     * @return void
     */
    public function testHandleAdded(): void
    {
        $this->testHandle(
            [
                'entity' => 'shelters',
                'delta_type' => 'added',
                'delta_ts' => time() - 15552000, // 6 months ago
                'exclude_software' => 'rescuegroups',
            ],
            10,
            true // test logging
        );
    }

    /**
     * Test handle for 'updated' delta_type
     *
     * @return void
     */
    public function testHandleUpdated(): void
    {
        $this->testHandle(
            [
                'entity' => 'shelters',
                'delta_type' => 'updated',
                'delta_ts' => time() - 15552000, // 6 months ago
            ]
        );
    }

    /**
     * Test handle for 'deleted' delta_type
     *
     * @return void
     */
    public function testHandleDeleted(): void
    {
        $partnerId = DB::table('shelter_partner_optout_map')
            ->where('optout_ts', '>=', date('Y-m-d h:i:s', time() - 15552000)) // 6 months ago
            ->orderBy('partner_id', 'DESC')
            ->first()
            ->partner_id;

        $this->testHandle(
            [
                'entity' => 'shelters',
                'delta_type' => 'deleted',
                'delta_ts' => time() - 15552000, // 6 months agoo
                'partner_id' => $partnerId
            ]
        );
    }

    /**
     * Test the special conditions for disaster-recovery apps for Hurricane
     * Harvey & Irma
     *
     * @return void
     */
    public function testHandleDisasterRecoveryApps(): void
    {
        $this->testHandle(
            [
                'partner_id' => Partner::where('tech_name', 'finding_rover')
                    ->first()
                    ->partner_id
            ]
        );
    }

    /**
     * Assertions on the the data returned from the formatShelterUrl method on
     * the BulkExportSheltersJob class
     *
     * @return void
     */
    public function testFormatShelterUrl(): void
    {
        $bulkExportSheltersJob = new BulkExportSheltersJob(
            new BulkExportJob(['partner_id' => $this->partnerId])
        );
        $formattedShelter = $bulkExportSheltersJob->formatShelterUrl(
            Shelter::where('website_url', 'like', '%facebook.com/%')->first()
        );
        $this->assertIsString($formattedShelter->facebook_url, 'Expect shelter to contain a facebook_url string');
    }
}

<?php

namespace Tests;

use Tests\TestCase;
use DB;

class TestCaseWithApiKey extends TestCase
{
    public $apiKey;

    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $partner = DB::table('public.partners')
            ->where('tech_name', 'aap_devs')
            ->first();
        $this->apiKey = $partner->api_key;
        $this->partnerId = $partner->partner_id;
    }
}

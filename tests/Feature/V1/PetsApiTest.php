<?php

namespace Tests\Feature\V1;

use Tests\TestCaseWithApiKey;

class PetsApiTest extends TestCaseWithApiKey
{
    protected $petId = 23266627;

    /**
     * Generate a random species
     *
     * @return string
     */
    protected function species()
    {
        return rand(0, 1) ? 'cat' : 'dog';
    }

    /**
     * Test search_form
     *
     * @return void
     */
    public function testSearchForm()
    {
        $params = http_build_query(
            [
                'key' => $this->apiKey,
                'species' => $this->species(),
            ]
        );
        $response = $this->getJson("/search/search_form?$params");
        $response->assertStatus(200)
            ->assertJson(['status' => 'ok']);
    }

    /**
     * Test pet_search
     *
     * @return void
     */
    public function testPetSearch()
    {
        $params = http_build_query(
            [
                'key' => $this->apiKey,
                'geo_range' => 50,
                'city_or_zip' => 84606,
                'species' => $this->species(),
                'limit' => 1,
                'breed_id' => 972,
            ]
        );
        $response = $this->getJson("/search/pet_search?$params");
        $response->assertStatus(200)
            ->assertJson(['status' => 'ok']);
    }

    /**
     * Assertions on the search/pet_details endpoint for the success state
     *
     * @return void
     */
    public function testPetDetailsSuccess()
    {
        $params = http_build_query(
            [
                'key' => $this->apiKey,
                'pet_id' => $this->petId,
            ]
        );
        $response = $this->getJson("/search/pet_details?$params");
        $response->assertStatus(200)
            ->assertJson(['status' => 'ok']);
    }

    /**
     * Assertions on the search/pet_details endpoint for the not found state
     *
     * @return void
     */
    public function testPetDetailsNotFound()
    {
        $params = http_build_query(
            [
                'key' => $this->apiKey,
                'pet_id' => -1, // bad pet_id
            ]
        );
        $response = $this->getJson("/search/pet_details?$params");
        $response->assertStatus(400)
            ->assertJson(['status' => 'fail']);
    }

    /**
     * Assertions on the search/limited_details endpoint for the success state
     *
     * @return void
     */
    public function testLimitedPetDetailsSuccess()
    {
        $params = http_build_query(
            [
                'key' => $this->apiKey,
                'pet_id' => $this->petId,
            ]
        );
        $response = $this->getJson("/search/limited_pet_details?$params");
        $response->assertStatus(200)
            ->assertJson(['status' => 'ok']);
    }

    /**
     * Assertions on the search/limited_pet_details endpoint for the not found
     * state
     *
     * @return void
     */
    public function testLimitedPetDetailsNotFound()
    {
        $params = http_build_query(
            [
                'key' => $this->apiKey,
                'pet_id' => -1, // bad pet_id
            ]
        );
        $response = $this->getJson("/search/limited_pet_details?$params");
        $response->assertStatus(400)
            ->assertJson(['status' => 'fail']);
    }
}

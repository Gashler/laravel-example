<?php

namespace Tests\Feature\V1;

use Tests\TestCaseWithApiKey;

class SheltersApiTest extends TestCaseWithApiKey
{
    protected $shelterId = 98957;

    /**
     * Test shelters
     *
     * @return void
     */
    public function testShelterSearch()
    {
        $params = http_build_query(
            [
                'key' => $this->apiKey,
                'geo_range' => 50,
                'city_or_zip' => 84606,
                'adopts_out' => 1,
            ]
        );
        $response = $this->getJson("/search/shelter_search?$params", );
        $response->assertStatus(200)
            ->assertJson(['status' => 'ok']);
    }

    /**
     * Assertions on the search/shelter_details endpoint for the success state
     *
     * @return void
     */
    public function testShelterDetailsSuccess()
    {
        $params = http_build_query(
            [
                'key' => $this->apiKey,
                'shelter_id' => $this->shelterId,
                'geo_range' => 50,
                'city_or_zip' => 84606,
                'adopts_out' => 1,
            ]
        );
        $response = $this->getJson("/search/shelter_details?$params");
        $response->assertStatus(200)
            ->assertJson(['status' => 'ok']);
    }

    /**
     * Assertions on the search/shelter_details endpoint for the not found state
     *
     * @return void
     */
    public function testShelterDetailsNotFound()
    {
        $params = http_build_query(
            [
                'key' => $this->apiKey,
                'shelter_id' => -1, // bad shelter_id
                'geo_range' => 50,
                'city_or_zip' => 84606,
                'adopts_out' => 1,
            ]
        );
        $response = $this->getJson("/search/shelter_details?$params");
        $response->assertStatus(400)
            ->assertJson(['status' => 'fail']);
    }

    /**
     * Assertions on the search/pets_at_shelter endpoint for the succss state
     *
     * @return void
     */
    public function testPetsAtShelterSuccess()
    {
        $params = http_build_query(
            [
                'key' => $this->apiKey,
                'shelter_id' => $this->shelterId,
            ]
        );
        $response = $this->getJson("/search/pets_at_shelter?$params");
        $response->assertStatus(200)
            ->assertJson(['status' => 'ok']);
    }

    /**
     * Assertions on the search/pets_at_shelter endpoint for the not found state
     *
     * @return void
     */
    public function testPetsAtShelterNotFound()
    {
        $params = http_build_query(
            [
                'key' => $this->apiKey,
                'shelter_id' => -1, // bad shelter_id
            ]
        );
        $response = $this->getJson("/search/pets_at_shelter?$params");
        $response->assertStatus(400)
            ->assertJson(['status' => 'fail']);
    }

    /**
     * Test pets at shelters
     *
     * @return void
     */
    public function testPetsAtShelters()
    {
        $params = http_build_query(
            [
                'key' => $this->apiKey,
                'shelter_ids' => [98957, 94752],
            ]
        );
        $response = $this->getJson("/search/pets_at_shelters?$params");
        $response->assertStatus(200)
            ->assertJson(['status' => 'ok']);
    }
}

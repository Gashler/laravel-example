<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\V2\BulkExportJob;
use App\Models\V1\Shelter;
use Faker\Generator as Faker;

$factory->define(
    BulkExportJob::class,
    function (Faker $faker) {
        $entities = ['pets', 'shelters'];
        $countries = ['US', 'US&CA'];
        $options = ['detailsUrl'];
        $filters = new stdClass;
        $filters->shelter_id = [Shelter::first()->shelter_id];
        $filters = [json_encode($filters)];
        $excludeSoftware = ['rescuegroups'];
        $deltaType = [null, 'updated', 'added', 'deleted'];

        return [
            'entity' => $entities[rand(0, count($entities) - 1)],
            'partner_id' => 4328,
            'country' => $countries[rand(0, count($countries) - 1)],
            'options' => $options[rand(0, count($options) - 1)],
            'filters' => $filters[rand(0, count($filters) - 1)],
            'exclude_software' => $excludeSoftware[rand(0, count($excludeSoftware) - 1)],
            'delta_ts' => time() - 86400, // last day
            'until_ts' => time(),
            'delta_type' => $deltaType[rand(0, count($deltaType) - 1)],
            'status' => 'pending'
        ];
    }
);

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\V1\PetSearchQuery;
use Faker\Generator as Faker;

$factory->define(PetSearchQuery::class, function (Faker $faker) {
    return [
        'bonded_pair' => false,
        'clan_name' => 'dog',
        'country_code' => 'US',
        'age' => 'young',
        'family_name' => 'Poodle (Miniature)',
        'lon_lat' => '(122.807,38.5438)',
        'postal_code' => '95492',
        'geo_range' => '100',
        'family_id' => '213',
        'geo_range_km' => '160',
        'is_cat' => false,
        'clan_id' => true,
        'state_code' => 'CA',
        'species' => 'dog'
    ];
});

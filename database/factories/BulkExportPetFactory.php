<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\V2\BulkExportPet;
use Faker\Generator as Faker;

$factory->define(BulkExportPet::class, function (Faker $faker) {
    return [
        'pet_contact_phone_number' => rand(200, 999) . '-' . rand(1000, 9999),
        'pet_contact_phone_area_code' => rand(100, 999),
        'pet_contact_phone_extension' => rand(1, 999),
        'youtube_video_id' => Str::random(11),
        'primary_breed' => 'Affenpinscher',
        'secondary_breed' => 'Afghan Hound',
        'mixed' => true,
        'coat_length' => 'medium'
    ];
});
